# -----------------------------------------------------------------------------
# JSatOrb project: Dockerization of the JSatOrb GUI Angular server
# -----------------------------------------------------------------------------
# NodeJS v11.15.0
# Angular CLI v8.3.20
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
# Multi-stage build: first image - Build the Angular application.
# -----------------------------------------------------------------------------

# Use NodeJS 11.15 base image
FROM node:11.15.0 AS builder

# Set working dir
WORKDIR /app

# Copy the application
COPY . .

# Install dependencies and build the application 
RUN npm install && \
    npm run ng build


# -----------------------------------------------------------------------------
# Multi-stage build: second image - Deploy the JSatOrb server into nginx.
# -----------------------------------------------------------------------------

# Use the latest nginx image
FROM nginx:alpine

# Copy the built JSatOrb server to the nginx server content folder
COPY --from=builder /app/dist/jsatorb-frontend/ /usr/share/nginx/html/

# Copy the JSatOrb specific nginx configuration
COPY nginx.conf /etc/nginx/conf.d/default.conf

# Expose Web port 80
EXPOSE 80

# Set the nginx server entry point
ENTRYPOINT [ "nginx", "-g", "daemon off;" ]


# Useful links
# https://mherman.org/blog/dockerizing-an-angular-app/

# docker build  . -t jsatorb-frontend:hotFix
# docker run -p 80:80 -it --rm jsatorb-frontend-luplink:hotfix


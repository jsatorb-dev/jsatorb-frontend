# JSatOrb project: Angular JSatOrb GUI (front-end)

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.1.3.
It contains the JSatOrb GUI which relies on the JSatOrb REST API to provide a simulator to ISAE/Supaero students.


## Prerequisites

___Note: the following pre-requisites are satisfied when installing the JSatOrb development environment detailed in the [JSatOrb developer installation documentation](../jsatorb/doc/dev/dev-install.md).___

- NodeJS and NPM are installed
- Angular CLI installed ``` npm install -g @angular/cli ```
- Angular Material DateTimePicker v2.0.4 installed ``` npm install --save @angular-material-components/datetime-picker@2.0.4 ```
    https://www.npmjs.com/package/@angular-material-components/datetime-picker
- Image cropper v3.1.8 for Angular installed ``` npm install ngx-image-cropper@3.1.8 --save ```
    https://www.npmjs.com/package/ngx-image-cropper
- Angular Material Moment Adapter v2.0.2 installed ``` npm install --save @angular-material-components/moment-adapter@2.0.2 ```

If currently working with an outdated datetime-picker version (initial version is 2.0.1), run the following commands: 
- ``` npm uninstall @angular-material-components/datetime-picker@2.0.1 ```,
- ``` npm install @angular-material-components/datetime-picker@2.0.4 ```,
- ``` npm ls @angular-material-components/datetime-picker ``` to check installation's effectiveness.


# Module installation

```
cd jsatorb-frontend
npm install
```


## Run the development server

To run an Angular server in the development environment:

- Run `ng serve` to run the Angular/JSatOrb GUI server.  
- Navigate to `http://localhost:4200/`.  
- The application will be automatically re-compiled if you change any of the source files.

___Note: in a JSatOrb user environment, the JSatOrb GUI server is ran from a Docker container located in the user installation folder.___
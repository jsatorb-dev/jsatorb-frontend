npm version
{
  'jsatorb-frontend': '1.0.0',
  npm: '8.19.3',
  node: '12.22.5',
  v8: '7.8.279.23-node.56',
  uv: '1.40.0',
  zlib: '1.2.11',
  brotli: '1.0.9',
  ares: '1.17.2',
  modules: '72',
  nghttp2: '1.41.0',
  napi: '8',
  llhttp: '2.1.3',
  http_parser: '2.9.4',
  openssl: '1.1.1k',
  cldr: '37.0',
  icu: '67.1',
  tz: '2019c',
  unicode: '13.0'
}



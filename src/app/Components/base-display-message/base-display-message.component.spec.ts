import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseDisplayMessageComponent } from './base-display-message.component';

describe('BaseDisplayMessageComponent', () => {
  let component: BaseDisplayMessageComponent;
  let fixture: ComponentFixture<BaseDisplayMessageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BaseDisplayMessageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseDisplayMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

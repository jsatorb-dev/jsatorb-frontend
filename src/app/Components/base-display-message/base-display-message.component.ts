import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-base-display-message',
  templateUrl: './base-display-message.component.html',
  styleUrls: ['./base-display-message.component.scss']
})

/**
 * Component used to centralize the message display functions (see below).
 * Each component using this one declares two divs in its HTML part and uses centralized styles which are in the 'styles.scss' file.
 */
export class BaseDisplayMessageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  public lastErrorMessage: string; // Variable used to show error messages, if any, when sending a request to the REST API.
  public lastInfoMessage: string; // Variable used to show informative messages, if any, when sending a request to the REST API.

/**
   * Setter.
   * @param msg The informative message to set.
   */
  setInfoMessage(msg: string) {
    this.lastErrorMessage = '';
    this.lastInfoMessage = msg;
    console.log(msg);
  }

  /**
   * Setter.
   * @param msg The error message to set.
   */
  setErrorMessage(msg: string) {
    this.lastErrorMessage = msg;
    this.lastInfoMessage = '';
    console.error(msg);
  }

  /**
   * Update the GUI operation message according to a returned status.
   * @param status The status to react to.
   */
  updateMessages(status: Map<string, string>) {
    if (status.has('status')) {
      if (status.get('status') == 'SUCCESS') {
        if (status.has('message')) {  // If a message exists in the status to display...
          this.setInfoMessage(status.get('message'));
        }
        // If no message associated with the SUCCESS status, no worry
      } else { // FAILURE
        if (status.has('message')) {  // If a message exists in the status to display...
          this.setErrorMessage(status.get('message'));
        } else {
          this.setErrorMessage('Unknown status received (status=FAIL but no message). Debug to check invalid status source !');
        }
      }  
    } else { // If not SUCCESS/FAIL status => Error message, by default, with a warning on the missing status value.
      if (status.has('message')) {  // If a message exists in the status to display...
        this.setErrorMessage(status.get('message') + ' (warning: no status (SUCCESS/FAIL) detected in the status to display)');
      } else { // Else, warn that an invalid status has been received.
        this.setErrorMessage('Unknown status received (no status, no message). Debug to check invalid status source !')
      }
    }
  }

}

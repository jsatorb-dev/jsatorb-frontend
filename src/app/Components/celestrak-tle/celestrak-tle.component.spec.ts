import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CelestrakTleComponent } from './celestrak-tle.component';

describe('CelestrakTleComponent', () => {
  let component: CelestrakTleComponent;
  let fixture: ComponentFixture<CelestrakTleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CelestrakTleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CelestrakTleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

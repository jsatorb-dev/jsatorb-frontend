import {Component, OnInit, ViewChild, AfterViewInit} from '@angular/core';
import {SelectionModel} from '@angular/cdk/collections';
import {MatPaginator, MatTableDataSource, MatSort} from '@angular/material';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, merge, Observable, of as observableOf} from 'rxjs';
import {catchError, map, startWith, switchMap} from 'rxjs/operators';
import {MissionService} from '../../Services/mission/mission.service';
import {TleSatModel} from '../../Models/TleSat.model';

@Component({
  selector: 'app-celestrak-tle',
  templateUrl: './celestrak-tle.component.html',
  styleUrls: ['./celestrak-tle.component.scss']
})
export class CelestrakTleComponent implements OnInit {
  displayedColumns: string[] = ['select', 'name'];
  dataSource = new MatTableDataSource<any>();
  selection = new SelectionModel<any>(true, []);

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;

  constructor(private httpClient: HttpClient, private missionService: MissionService) { }

  ngOnInit() {
    //this.httpClient.get<any[]>('http://localhost:7777/NORAD/elements/active.txt').subscribe(response => {
      this.httpClient.get<any[]>('http://localhost:7777/NORAD/elements/gp.php?GROUP=active&FORMAT=tle').subscribe(response => {
      this.dataSource = new MatTableDataSource<any[]>(response);
      this.dataSource.paginator = this.paginator;
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  valide() {
    this.selection.selected.forEach((tle: any) => {
      this.missionService.addSatellite(new TleSatModel(tle.name, 'tle', tle.line1, tle.line2));
    });
  }
}

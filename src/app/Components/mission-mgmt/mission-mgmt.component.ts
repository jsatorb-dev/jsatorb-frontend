import { Component, OnInit, OnDestroy } from '@angular/core';
import { MissionService } from 'src/app/Services/mission/mission.service';
import { Subscription } from 'rxjs';
import { BaseDisplayMessageComponent } from '../base-display-message/base-display-message.component';
import { MatDialog } from '@angular/material';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';

@Component({
  selector: 'app-mission-mgmt',
  templateUrl: './mission-mgmt.component.html',
  styleUrls: ['./mission-mgmt.component.scss']
})
export class MissionMgmtComponent extends BaseDisplayMessageComponent implements OnInit, OnDestroy {

  /**
   * Definition of authorized characters in the mission name input field.
   */
  MISSION_NAME_AUTHORIZED_CHAR: string = '[0-9a-zA-Z\-\.]';
  MISSION_NAME_AUTHORIZED_PASTED_STRING: string = '^[0-9a-zA-Z\-\.]{1,30}$';

  // Mission name to store in the back-end.
  missionName: string;

  // List of available mission data sets.
  missionDataSetList: string[];
  missionDataSetListSubscription : Subscription;

  // Current selected mission for actions: load, delete, duplicate
  selectedMission: string;

  // Subscribe to the current mission name
  currentMissionSubscription: Subscription;

  constructor(private dialog: MatDialog, private missionService: MissionService) {
    super();
  }

  ngOnInit() {
    console.log("MISSION-MGMT ONINIT()");
    this.missionName = this.missionService.getCurrentMission();
  
    // Subscribe to the current mission events
    this.missionDataSetListSubscription = this.missionService.missionDataSetListSubject.subscribe(missionDataSetList => this.missionDataSetList = missionDataSetList);

    this.missionService.getMissionDataSetList().then(
      (list: string[]) => {
        this.missionDataSetList = list;
      },
      (error: ProgressEvent) => {
        this.setErrorMessage("An error occured while retrieving the mission data set list: " + error['message']);
      }
    );

    // Subscribe to the current mission events
    // When a new mission data set is loaded, then our local missionName property is updated.
    this.currentMissionSubscription = this.missionService.currentMissionSubject.subscribe(currentMission => this.missionName = currentMission);
  }

  ngOnDestroy(): void {
    this.missionDataSetListSubscription.unsubscribe();
  }

  /**
   * Setter.
   * @param msg The informative message to set.
   */
  setInfoMessage(msg: string) {
    this.lastErrorMessage = '';
    this.lastInfoMessage = msg;
  }

  /**
   * Setter.
   * @param msg The error message to set.
   */
  setErrorMessage(msg: string) {
    this.lastErrorMessage = msg;
    this.lastInfoMessage = '';
  }

  /**
   * Method triggered when a click occurs on the Save button.
   */
  onSaveAsMission() {
    this.missionService.saveMissionData(this.missionName).then((status: Map<string, string>) => {
      this.updateMessages(status);
    });
  }

  /**
   * Method triggered when a click occurs on the Load button.
   */
  onLoadMission() {
    this.missionService.loadMissionData(this.selectedMission).then((status: Map<string, string>) => {
      this.updateMessages(status);
    });
  }

  /**
   * Method triggered when a click occurs on the Delete button.
   */
  onDeleteMission() {
    const dialogRef = 
      this.dialog.open(
        ConfirmationDialogComponent, 
        {
          width: '350 px',
          data: "Are you sure that you want to delete the mission data set named '" + this.selectedMission + "' ?"
        });

        // After dialog has been closed, react to the user choice
        dialogRef.afterClosed().subscribe( result => {
          if (result) {
            this.missionService.deleteMissionData(this.selectedMission).then((status: Map<string, string>) => {
              this.updateMessages(status);
              if (status.get('status') == 'SUCCESS') {
                this.selectedMission = '';
              }
            });
          }
        });
  }

  /**
   * This method controls the characters in the mission name input field,
   * when a key is stroked.
   * Only a few characters are authorized as this mission name is used in a filename in the JSatOrb back-end.
   * 
   * So, this method test the current event input key and prevent its input if it doesn't 
   * match the authorized characters list (a regex pattern).
   * 
   * @param event The key input event
   */
  onAddCharacterInMissionInput(event: any){
    const pattern = new RegExp(this.MISSION_NAME_AUTHORIZED_CHAR);
    let inputChar = event.key;

    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      event.preventDefault();
    }
  }

  /**
   * This method controls the characters in the mission name input field,
   * when a text is pasted.
   * Only a few characters are authorized as this mission name is used in a filename in the JSatOrb back-end.
   * 
   * So, this method test the current event input key and prevent its input if it doesn't 
   * match the authorized characters list (a regex pattern).
   * 
   * @param event The key input event
   */
  onPasteInMissionInput(event: ClipboardEvent){
    const pattern = new RegExp(this.MISSION_NAME_AUTHORIZED_PASTED_STRING);
    let clipboardData = event.clipboardData;
    let pastedText = clipboardData.getData('text');

     if (!pattern.test(pastedText)) {
       // invalid text, prevent input
       event.preventDefault();
     }
  }
}

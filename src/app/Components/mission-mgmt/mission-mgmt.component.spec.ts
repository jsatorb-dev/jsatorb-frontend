import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MissionMgmtComponent } from './mission-mgmt.component';

describe('MissionMgmtComponent', () => {
  let component: MissionMgmtComponent;
  let fixture: ComponentFixture<MissionMgmtComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MissionMgmtComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MissionMgmtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

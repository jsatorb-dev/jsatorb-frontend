import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EclipseChartComponent } from './eclipse-chart.component';

describe('EclipseChartComponent', () => {
  let component: EclipseChartComponent;
  let fixture: ComponentFixture<EclipseChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EclipseChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EclipseChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

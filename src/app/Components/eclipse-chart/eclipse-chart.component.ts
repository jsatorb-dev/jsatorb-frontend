import {Component, Input, OnInit} from '@angular/core';
import {Chart, ChartData} from 'chart.js';
@Component({
  selector: 'app-eclipse-chart',
  templateUrl: './eclipse-chart.component.html',
  styleUrls: ['./eclipse-chart.component.scss']
})
export class EclipseChartComponent implements OnInit {
  _data: any;
  canvas: any;
  ctx: any;
  constructor() { }
  options = {
  scales: {
    xAxes: [{
      type: 'time',
      time: {
        unit: 'minute'
      }
    }]
  }
};

config = {
  type:    'line',
  data: {
    datasets: [
      {
        label: 'eclipse',
        data: [],
        fill: false,
        borderColor: 'red'
      }]
  },
  options: {
    scales: {
      xAxes: [{
        type: 'time',

      }]
    },
    elements: {
      line: {
        tension: 0
      }
    }
  }
};

  @Input()
  get data(): any {
    return this._data;
  }
  set data(data) {
    this._data = data;
  }
ngOnInit() {
  this.canvas = document.getElementById('myChart');
  this.ctx = this.canvas.getContext('2d');
  this.data.forEach((el) => {

    this.config.data.datasets[0].data.push({
      x: new Date(el.start),
      y: 0
    });
    this.config.data.datasets[0].data.push({
      x: new Date(el.start),
      y: 1,
    });
    this.config.data.datasets[0].data.push({
      x: new Date(el.end),
      y: 1
    });

    this.config.data.datasets[0].data.push({
      x: new Date(el.end),
      y: 0
    });

  });
  const myChart = new Chart(this.ctx, this.config);
}

}

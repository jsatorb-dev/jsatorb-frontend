import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ThemePalette } from '@angular/material/core';
import { BaseDisplayMessageComponent } from '../base-display-message/base-display-message.component';
import { ConstantsService } from 'src/app/Services/mission/constants.service';

@Component({
  selector: 'app-date-conversion',
  templateUrl: './date-conversion.component.html',
  styleUrls: ['./date-conversion.component.scss']
})

/**
 * Date conversion component.
 * 
 * With a date and time picker input and a selection between JD and MJD output format.
 */
export class DateConversionComponent extends BaseDisplayMessageComponent implements OnInit {

  dateConversionForm: FormGroup; // The Date conversion Form group
  dateToConvert: Date; // Picked date to convert
  dateTargetFormat: string; // Target format to convert to (JD, MJD)
  dateConvertedValue: string; // Final converted value as returned by the REST API.

  /** Date/Time picker variables */
  public disabled = false;
  public showSpinners = true;
  public showSeconds = true;
  public touchUi = false;
  public enableMeridian = false;
  public stepHour = 1;
  public stepMinute = 1;
  public stepSecond = 1;
  public color: ThemePalette = 'primary';

  public listColors = ['primary', 'accent', 'warn'];

  public stepHours = [1, 2, 3, 4, 5];
  public stepMinutes = [1, 5, 10, 15, 20, 25];
  public stepSeconds = [1, 5, 10, 15, 20, 25];

    // Http client to dialog with the REST API.
  constructor(private formBuilder: FormBuilder, private httpClient: HttpClient) {
    super();
   }

  // Class initialization code
  ngOnInit() {
    this.dateToConvert = new Date();
    this.initForm();
  }

  // Form initialization code
  initForm() {
    this.dateConversionForm = this.formBuilder.group(
      {
        dateToConvert:['',Validators.required],
        dateTargetFormat:['',Validators.required]
      }
    );
  }

  /**
   * Called to get the converted date from the REST API given the user inputs.
   */
  onRunningCalculation() {
    // Get the form values.
    const dateConversionValue = this.dateConversionForm.value;
    const rawDate = dateConversionValue['dateToConvert'];
    const dateISOFormat = ConstantsService.formatDateISO8601(rawDate);

    const dateTargetFormat = dateConversionValue['dateTargetFormat'];
    this.dateTargetFormat = dateTargetFormat;

    // // Debug traces: Date and target format
    // console.log("Date=" + dateISOFormat);
    // console.log("Target format=" + dateTargetFormat);

    // Build the HTTP Request intended to the REST API (route: dateconversion).
    let req = {
      'header': {
        'dateToConvert': dateISOFormat,
        'targetFormat': dateTargetFormat
      }
    };

    /**
     * Post the request to the REST API (route: dateconversion) and get the resulting value.
     * Request format: see the map above; all fields are stored in a 'header'.
     * Response format: a map containing a unique key/value pair. 
     *    Key is 'dateConverted' and 
     *    Value is a string with the converted date value in JD or MJD according to the output date format givan in the request.
     * 
     */ 
    this.httpClient.post('http://localhost:8000/dateconversion', req)
    .toPromise()
    .then((res: any) => {
      const status = res['status'];
      const message = res['message'];
      const data = res['data'];

      if (status == 'SUCCESS') {
        const dateConverted = data['dateConverted'];

        // Display value unit according to the output format.
        if (this.dateTargetFormat == 'JD') {
          this.dateConvertedValue = dateConverted.toString() + " Julian Days (JD)";
        } else if (this.dateTargetFormat == 'MJD') {
          this.dateConvertedValue = dateConverted.toString() + " Modified Julian Days (MJD)";
        } else {
          this.dateConvertedValue = "";
        }
        this.setInfoMessage('Request successfully sent to the JSatOrb REST API.');
      } else {
        this.setErrorMessage("An error occured whiele sending the request to the REST API: " + message);
      }
    },
    (error: ProgressEvent) => {
      const msg = error['message'];
      console.error(msg);
      this.setErrorMessage(msg);
    });
  }  
}

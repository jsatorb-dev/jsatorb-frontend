import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DateConversionComponent } from './date-conversion.component';

describe('DateConversionComponent', () => {
  let component: DateConversionComponent;
  let fixture: ComponentFixture<DateConversionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DateConversionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DateConversionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

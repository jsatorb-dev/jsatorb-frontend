import {Component, OnInit, OnDestroy, SystemJsNgModuleLoader} from '@angular/core';
import {ConstellationModel} from '../../Models/Constellation.model';
import {MissionService} from '../../Services/mission/mission.service';
import {ActivatedRoute, Router} from '@angular/router';
import { Subscription } from 'rxjs';
import { BaseDisplayMessageComponent } from '../base-display-message/base-display-message.component';
import { SatelliteModel } from 'src/app/Models/Satellite.model';

@Component({
  selector: 'app-constellation-form',
  templateUrl: './constellation-form.component.html',
  styleUrls: ['./constellation-form.component.scss']
})
export class ConstellationFormComponent extends BaseDisplayMessageComponent implements OnInit, OnDestroy {

  // Constellation's name
  consName: string = ''; 

  // Selected sat to add to the constellation.
  selectedSat: string = '';

  // List of satellites of the constellation.
  satellitesInConsList: string[] = []; 

  routeSubscription: Subscription;

  satelliteListSubscription : Subscription;
  constellationListSubscription : Subscription;

  constructor(private missionService: MissionService, private router: Router, private route: ActivatedRoute) {
    super();
  }

  ngOnInit() {
    // If there is a constellation name part in the route used to create the current component, 
    // we initialize the form with the constellation attributes.
    this.routeSubscription = this.route.params.subscribe(
      params => {
        const consName = params['name'];

        if (consName !== undefined) {
          const constellation = this.missionService.getConstellationByName(consName);
    
          if (constellation !== undefined) {
            this.consName = constellation.name;
            // One-depth deep copy the list of satellites
            var satlist: string[] = [];
            constellation.satellites.forEach( (satname) => satlist.push(satname) );

            this.satellitesInConsList = satlist;
          }
        }
      });

    // Subscribe to the satellites list update events
    this.satelliteListSubscription = this.missionService.satelliteListSubject.subscribe((satList: SatelliteModel[]) => {

        /**
         * Update the satellites list in case a satellite has been removed from the mission.
         */
        this.satellitesInConsList = this.satellitesInConsList.filter( sat => this.missionService.getSatelliteByName(sat) !== undefined );
    });

    // Subscribe to the constellations list update events
    this.constellationListSubscription = this.missionService.constellationListSubject.subscribe((constList: ConstellationModel[]) => {      

      const consName = this.consName;
      // If a constellation is currently edited.
      if (consName !== undefined && consName !== null && consName !== "") {

        /**  
         * Check if the currently edited constellation has not been removed from the mission.
         */

        // Look for its name in the mission constellations list.
        const foundConst = this.missionService.getConstellations().find( constellation => constellation.name === consName );

        // If the constellation has not been found, re-route the current content to the 'new constellation' form.
        if (foundConst === undefined || foundConst === null) {
          setTimeout(
            () => {
              this.router.navigate(['/constellation']);
            }, 
            2000
          );
        }
      }

    });
  }

  ngOnDestroy(): void {
    this.constellationListSubscription.unsubscribe();
    this.routeSubscription.unsubscribe();
    }

  /**
   * Check if a satellite is selected in the available satellites list.
   * @returns true if one is selected, false otherwise.
   */
  public isSelectedSat(): boolean {
    return this.selectedSat != undefined && this.selectedSat != '';
  }

  /**
   * Check if there is at least one satellite to add to the constellation.
   * @returns True if there is at least one, false otherwise.
   */
  public isThereSatellitesToAdd(): boolean {

    let alo = true; // alo = at least one

    const allSats = this.missionService.getSatellites();
    // No satellite in mission => false
    if (allSats == undefined || allSats == null || allSats.length == 0) {
      alo = false;
    } else {
      let asaitc = true; // asaitc = all sats are in the constellation

      // Check if all mission satellitesa re in the constellation yet.
      allSats.forEach(element => {
        if (!this.satellitesInConsList.includes(element.name)) {
          asaitc = false;
        }
      });
      alo = !asaitc;
    }
    return alo;
  }

  /**
   * Add the selected satellite to the constellation.
   */
  public addSat() {
    if (this.isSelectedSat()) {
      this.satellitesInConsList.push(this.selectedSat);
      // "Unselect" the newly added satellite (cannot be added twice).
      this.selectedSat = undefined;
    }
  }

  /**
   * Add all the available satellites to the constellation.
   */
  public addAllSats() {
    const allSats = this.missionService.getSatellites();

    // Check if there is at least one satellite in the mission.
    if (allSats !== undefined && allSats != null && allSats.length > 0) {
      // Reset the current list of sa in the constellation.
      this.satellitesInConsList = [];

      // Add each mission sat to the constellation.
      allSats.forEach(element => {
        if (!this.satellitesInConsList.includes(element.name)) {
          this.satellitesInConsList.push(element.name);
        }
      });
      // "Unselect" the newly added satellite (cannot be added twice).
      this.selectedSat = undefined;
    }
  }

  /**
   * Remove a satellite from the constellation.
   * @param sat The satellite to remove.
   */
  public removeSat(sat) {
    const index = this.satellitesInConsList.indexOf(sat, 0);
    if (index > -1) {
      this.satellitesInConsList.splice(index, 1);
    }
  }

  /**
   * Get the list of available satellite names.
   * An available satellite is a satellite not already present in the constellation.
   */
  public getAvailableSatellites(): string[] {
    const allSats = this.missionService.getSatellites();
    const availableSats: string[] = [];
    allSats.forEach(element => {
      if (!this.satellitesInConsList.includes(element.name)) {
        availableSats.push(element.name);
      }
    });
    return availableSats;
  }

  /**
   * Get the list of satellites the constellation contains.
   */
  public getSatellitesInConstellation(): string[] {
    return this.satellitesInConsList;
  }

  /**
   * Store the constellation in the mission data set.
   */
  public onSaveConstellation() {
    // One-depth deep copy the list of satellites
    var satlist: string[] = [];
    this.satellitesInConsList.forEach( (satname) => satlist.push(satname) );

    this.missionService.addConstellation(new ConstellationModel(this.consName, satlist));

    // Update the stored mission data set.
    this.missionService.saveCurrentMissionData().then(
      (status: Map<string, string>) => {

          // All messages land here (even errors).
          this.updateMessages(status);

          if (status.get('status') == 'SUCCESS') {
            setTimeout(
              () => {
              // Now the object is created, go to its own URL, 
              // to enable correct management of a possible new object creation.
              this.router.navigate(['/constellation/' + this.consName]);
            }, 
              2000
            );
          }
        }
    );
  }

  /**
   * Display the constellation wizard.
   */
  public onCreateConstellation() {
    setTimeout(
      () => {
        this.router.navigate(['/constellationWizard']);
      }, 
      2000
    );
  }
}

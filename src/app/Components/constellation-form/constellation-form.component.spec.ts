import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConstellationFormComponent } from './constellation-form.component';

describe('ConstellationFormComponent', () => {
  let component: ConstellationFormComponent;
  let fixture: ComponentFixture<ConstellationFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConstellationFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConstellationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

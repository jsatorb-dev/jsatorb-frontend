import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VtsVisualizationFormComponent } from './vts-visualization-form.component';

describe('VtsVisualizationFormComponent', () => {
  let component: VtsVisualizationFormComponent;
  let fixture: ComponentFixture<VtsVisualizationFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VtsVisualizationFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VtsVisualizationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

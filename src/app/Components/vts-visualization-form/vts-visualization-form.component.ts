import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MissionService } from 'src/app/Services/mission/mission.service';
import { HttpClient } from '@angular/common/http';
import { BaseDisplayMessageComponent } from '../base-display-message/base-display-message.component';
import { ConstantsService } from 'src/app/Services/mission/constants.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-vts-visualization-form',
  templateUrl: './vts-visualization-form.component.html',
  styleUrls: ['./vts-visualization-form.component.scss']
})
export class VtsVisualizationFormComponent extends BaseDisplayMessageComponent implements OnInit, OnDestroy {

  vtsVisualizationForm: FormGroup; // The VTS visualization Form group

  genCartesianEphemerid: boolean = true;
  genKeplerianEphemerid: boolean = false;
  genLatLonAltEphemerid: boolean = false;
  genEclipse: boolean = false;
  genVisibility: boolean = false;
  genAttitude: boolean = false;
  selectedAttitudeLaw: string = '';

  // List of available attitude laws.
  attitudeLaws: string[];

  optionsSubscription: Subscription;

  constructor(private formBuilder: FormBuilder, private missionService: MissionService, private httpClient: HttpClient) {
    super();
  }

  ngOnInit() {
    this.initForm();
  }

  /**
   * Update local options state.
   * @param options Options values to update to.
   */
  updateOptions(options: object) {
    if (options !== undefined) {
      this.genCartesianEphemerid = true;
      this.genKeplerianEphemerid = options.hasOwnProperty('KEPLERIAN')
      this.genLatLonAltEphemerid = options.hasOwnProperty('LLA')
      this.genEclipse = options.hasOwnProperty('ECLIPSE')
      this.genVisibility = options.hasOwnProperty('VISIBILITY')

      // Update attitude properties.
      this.genAttitude = options.hasOwnProperty('ATTITUDE');
      if (this.genAttitude) {
        const attitude = options['ATTITUDE'];
        // Check if the law parameter exists.
        if (attitude.hasOwnProperty('law')) {
          this.selectedAttitudeLaw = attitude['law'];
          console.log("selectedAttitudeLaw=" + this.selectedAttitudeLaw);
          console.log("OPTIONS=");
          console.log(options);
        }
      }
    }
  }

  // Form initialization code
  initForm() {
    // Get list of celestial bodies.
    this.attitudeLaws = ConstantsService.ATTITUDE_LAWS;

    this.vtsVisualizationForm = this.formBuilder.group(
      {
        genCartesianEphemerid:['',Validators.required],
        genKeplerianEphemerid:['',Validators.required],
        genLatLonAltEphemerid:['',Validators.required],
        genEclipse:['',Validators.required],
        genVisibility:['',Validators.required],
        genAttitude:['',Validators.required],
        selectedAttitudeLaw:['']
      }
    )
 
    // Update options according to the mission service current state.
    const options = this.missionService.getOptions();
    this.updateOptions(options);

    // Subscribe to options update events
    this.optionsSubscription = this.missionService.optionsSubject.subscribe((options: object) => {
      this.updateOptions(options);
    });
  }

  ngOnDestroy(): void {
    this.optionsSubscription.unsubscribe();
  }

  /**
   * Get the description of an attitude law.
   * @param law The law to get the description of.
   */
  getAttitudeLawDescription(law) : string {
    const desc = ConstantsService.ATTITUDE_LAWS_DESCRIPTIONS[law];
    return desc;
  }

  /**
   * Check if correct values are available to launch a VTS visualization
   * @return true if the data set is valid, false otherwise.
   */ 
  isMissionDataSetValid() {
    let valid: boolean = true;
    let messages: string[] = [];

    const ts = this.missionService.getTimeSettings();
    const sats = this.missionService.getSatellites();
    const cb = this.missionService.getCelestialBody();

    //Check if Time settings are defined.
    if (ts == undefined) {
      messages.push("Time settings are not correctly defined !");
      valid = false;
    } else if (ts.dateStart == undefined || ts.dateEnd == undefined) {
      messages.push("Time settings are not correctly defined (one date is undefined) !");
      valid = false;
    } else if (ts.dateStart.toString() == "" || ts.dateEnd.toString() == "") {
      messages.push("Time settings are not correctly defined (one date is undefined) !");
      valid = false;
    } else if (new Date(ts.dateStart).getTime() >= new Date(ts.dateEnd).getTime()) {
      messages.push("Time settings are not correctly defined (start date is equal or after end date) !");
      valid = false;
    } else if (ts.stepTime == undefined || ts.stepTime.toString() == "") {
      messages.push("Time settings are not correctly defined (step is undefined) !");
      valid = false;
    }

    // Check if Satellites list contains at least one satellite.
    if (sats == undefined || sats.length < 1) {
      messages.push("The mission data set contains no satellite !");
      valid = false;
    }

    // Check if the celestial body is set.
    if (cb == undefined || cb == '') {
      messages.push("The celestial body is not defined !");
      valid = false;
    }

    // If the attitude option is activated, check if the attitude law is defined.
    const formValue = this.vtsVisualizationForm.value;
    const genAttitude = formValue['genAttitude'];
    const selectedAttitudeLaw = formValue['selectedAttitudeLaw'];
    if (genAttitude) {
      if (!selectedAttitudeLaw) {
          messages.push("The Attitude option is activated, but no attitude law has been selected !");
          valid = false;
        }
    }

    var invalidInputParameters = '';
    // Concatenate messages, one by line.
    for (let msg in messages) {
      invalidInputParameters = invalidInputParameters.concat(messages[msg]);
      invalidInputParameters = invalidInputParameters.concat('\n');
    }

    this.setErrorMessage(invalidInputParameters);

    return valid;
  } 

  /**
   * Update the current VTS visualization options according to the user input.
   * Store them in the mission service and in the backend.
   * 
   * @returns The options array.
   */
  saveOptions(): object {
    // Get the form values.
    const formValue = this.vtsVisualizationForm.value;
    const genCartesianEphemerid = formValue['genCartesianEphemerid'];
    const genKeplerianEphemerid = formValue['genKeplerianEphemerid'];
    const genLatLonAltEphemerid = formValue['genLatLonAltEphemerid'];
    const genEclipse = formValue['genEclipse'];
    const genVisibility = formValue['genVisibility'];
    const genAttitude = formValue['genAttitude'];
    const selectedAttitudeLaw = formValue['selectedAttitudeLaw'];

    const options = {};

    /**
     * Build the VTS data generation options array.
     */
    if (genCartesianEphemerid) options['CARTESIAN'] = [];
    if (genKeplerianEphemerid) options['KEPLERIAN'] = [];
    if (genLatLonAltEphemerid) options['LLA'] = [];
    if (genEclipse) options['ECLIPSE'] = [];
    if (genVisibility) options['VISIBILITY'] = [];
    if (genAttitude) {
      options['ATTITUDE'] = {'law': selectedAttitudeLaw };
    }

    // Update options in the mission service.
    this.missionService.updateOptions(options);

    // Update options in the stored mission data set.
    this.missionService.saveCurrentMissionData().then((status: Map<string, string>) => {
      this.updateMessages(status);
    });

    console.log(options);

    return options;
  }

  /**
   * Check the mission data set and launch VTS to visualize it.
   */
  launchVTSVisualization() {

    // Check the mission data set before launching VTS.
    if (!this.isMissionDataSetValid()) {
      return;
    }

    /**
     * Get the VTS visualization user selected options and
     * store them in the mission data set in the same move.
     */
    const options = this.saveOptions();

    // Build the HTTP Request intended to the REST API.
    let req = this.missionService.buildJSatOrbRESTRequestWithOptions(options);

    console.log("REQUEST=" + req);

    const httpOptions = {
      responseType: 'blob' as 'json',
      contentType: 'application/vnd+cssi.vtsproject+zip' as const,
      observe: 'response' as const
    };
  
    this.httpClient.post('http://localhost:8000/vts', req, httpOptions).subscribe((data: any) => {

      // The HTTP reponse headers
      var headers = data.headers;

      // Content-type should be 'application/vnd+cssi.vtsproject+zip'
      var contentType = headers.get('Content-Type')
      var respType = 'application/vnd+cssi.vtsproject+zip';

      if (contentType !== null) {
        respType = contentType;
      }

      // String format: "attachment; filename='vts-<mission-name>-content.vz'"
      var contentDisposition = headers.get('Content-Disposition')
      var filename = "vts-content.vz"
      var mission = 'Unknown';

      const FILENAME = "filename='";
      if (contentDisposition !== null && contentDisposition.match("attachment; filename='.+'")) {
        // Extract the received data recommended filename.
        filename = contentDisposition.substring(contentDisposition.indexOf(FILENAME) + FILENAME.length, contentDisposition.length - 1);

        // Get the mission name (not used yet).
        if (filename.match("vts-.+-content.vz")) {
          const VTS = 'vts-';
          const CONTENT = '-content.vz';
          // Extract the mission name.
          mission = filename.substring(filename.indexOf(VTS) + VTS.length, filename.length - CONTENT.length);
        }
      }

      // The HTTP response body (blob containing the VTS compressed data)
      var respBody = data.body

      var blob = new Blob([respBody], { type: respType });
      var url = window.URL.createObjectURL(blob);

      // Generate an HTML link in memory on which we trigger the click event.
      var link = document.createElement('a');
      link.href = url;
      link.download = filename;
      link.type = respType;
      link.rel = 'stylesheet';
      link.click();
    
    });
  }
}

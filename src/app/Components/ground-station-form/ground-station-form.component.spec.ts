import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroundStationFormComponent } from './ground-station-form.component';

describe('GroundStationFormComponent', () => {
  let component: GroundStationFormComponent;
  let fixture: ComponentFixture<GroundStationFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroundStationFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroundStationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

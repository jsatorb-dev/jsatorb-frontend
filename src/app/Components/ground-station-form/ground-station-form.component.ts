import {Component, OnInit} from '@angular/core';
import {GroundStationModel} from '../../Models/GroundStation.model';
import {MissionService} from '../../Services/mission/mission.service';
import {ActivatedRoute, Router} from '@angular/router';
import {CzmlService} from '../../Services/czml/czml.service';
import { Subscription } from 'rxjs';
import { BaseDisplayMessageComponent } from '../base-display-message/base-display-message.component';

@Component({
  selector: 'app-ground-station-form',
  templateUrl: './ground-station-form.component.html',
  styleUrls: ['./ground-station-form.component.scss']
})
export class GroundStationFormComponent extends BaseDisplayMessageComponent implements OnInit {

  gsName: string = null;
  latitude: number = null;
  longitude: number = null;
  altitude: number = null;
  elevation: number = null;

  routeSubscription: Subscription;
  groundstationListSubscription: Subscription;

  constructor(private missionService: MissionService, private router: Router, private testService: CzmlService, private route: ActivatedRoute) {
    super();
  }

  ngOnInit() {
    this.routeSubscription = this.route.params.subscribe(
      params => {
        const gsName = params['name'];
        if (gsName !== undefined) {
          const station = this.missionService.getGroundStationByName(gsName);
          this.gsName = station.name;
          this.elevation = station.elevation;
          this.altitude = station.altitude;
          this.latitude = station.latitude;
          this.longitude = station.longitude;
        }
      }
    );

    // Subscribe to the satellites list update events
    this.groundstationListSubscription = this.missionService.groundStationListSubject.subscribe((gsList: GroundStationModel[]) => {

      /**
       * If the ground station currently edited is no more to be found in the mission, it means it just has been removed.
       */
      const gsname = this.gsName;
      if (gsname !== undefined && gsname !== null && gsname !== "") {

        // Look for its name in the mission ground station list.
        const foundGs = this.missionService.getSatellites().find( gs => gs.name === gsname );

        // If the ground station has not been found, re-route the current content to the 'new ground station' form.
        if (foundGs === undefined || foundGs === null) {
          setTimeout(
            () => {
              this.router.navigate(['/groundStation']);
            }, 
            2000
          );
        }
      }
    });
  }

  ngOnDestroy(): void {
    this.routeSubscription.unsubscribe();
  }

  public onSavingGs() {
    this.missionService.addGroundStation(new GroundStationModel(this.gsName, this.latitude, this.longitude, this.altitude
      , this.elevation));

    this.testService.addGroundStation(this.gsName, this.latitude, this.longitude, this.altitude);

    // Update the stored mission data set.
    this.missionService.saveCurrentMissionData().then(
      (status: Map<string, string>) => {

          // All messages land here (even errors).
          this.updateMessages(status);

          if (status.get('status') == 'SUCCESS') {
            setTimeout(
              () => {
              // Now the object is created, go to its own URL, 
              // to enable correct management of a possible new object creation.
              this.router.navigate(['/groundStation/' + this.gsName]);
            }, 
              2000
            );
          }
        }
    );
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LinkBudgetFormComponent } from './link-budget-form.component';

describe('LinkBudgetFormComponent', () => {
  let component: LinkBudgetFormComponent;
  let fixture: ComponentFixture<LinkBudgetFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LinkBudgetFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LinkBudgetFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

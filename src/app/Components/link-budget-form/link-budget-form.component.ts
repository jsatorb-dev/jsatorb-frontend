
import { Component, OnInit } from '@angular/core';

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-link-budget-form',
  templateUrl: './link-budget-form.component.html',
  styleUrls: ['./link-budget-form.component.scss']
})
export class LinkBudgetFormComponent implements OnInit {

  constructor(private httpClient: HttpClient) { }
  GS_altitude: number = null;
  GS_minElevation: number = null;
  SC_altitude: number = null;

  linkBugdetSynthesis: string = null;
  ngOnInit() {
  }

  public computeLB() {
   const a = {
      "GS_altitude": this.GS_altitude,
      "GS_minElevation": this.GS_minElevation,
      "SC_altitude": this.SC_altitude
   }
   console.log(a);

   this.httpClient
   .post('http://localhost:8000/linkBudget/Eb_N0', a)
   .subscribe(
     (elem) => {
       console.log('Enregistrement terminé !');
       console.log(elem);  
       console.log(elem['toto']);         
       this.linkBugdetSynthesis = JSON.stringify(elem);      
     },

     (error) => {

       console.log('Erreur ! : ' + error);

     }


   );

  }
}



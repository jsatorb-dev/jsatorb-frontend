import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VtsCoverageVisualizationFormComponent } from './vts-coverage-visualization-form.component';

describe('VtsCoverageVisualizationFormComponent', () => {
  let component: VtsCoverageVisualizationFormComponent;
  let fixture: ComponentFixture<VtsCoverageVisualizationFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VtsCoverageVisualizationFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VtsCoverageVisualizationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

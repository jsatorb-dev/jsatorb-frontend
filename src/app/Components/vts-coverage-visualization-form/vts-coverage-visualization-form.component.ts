import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MissionService } from 'src/app/Services/mission/mission.service';
import { HttpClient } from '@angular/common/http';
import { BaseDisplayMessageComponent } from '../base-display-message/base-display-message.component';
import { SatelliteModel } from 'src/app/Models/Satellite.model';
import { Subscription } from 'rxjs';
import { ConstellationModel } from 'src/app/Models/Constellation.model';
import { ImageCroppedEvent, CropperPosition } from 'ngx-image-cropper';
import { RegionModel } from 'src/app/Models/Region.model';
import { CoverageSettingsModel } from 'src/app/Models/CoverageSettings.model';
import { CoverageService } from 'src/app/Services/mission/coverage.service';
import { ThemePalette } from '@angular/material/core';

@Component({
  selector: 'app-vts-coverage-visualization-form',
  templateUrl: './vts-coverage-visualization-form.component.html',
  styleUrls: ['./vts-coverage-visualization-form.component.scss']
})
export class VtsCoverageVisualizationFormComponent extends BaseDisplayMessageComponent implements OnInit, OnDestroy {

  vtsCoverageVisualizationForm: FormGroup; // The VTS Coverage visualization Form group

  /** Date/Time picker variables */
  public disabled = false;
  public showSpinners = true;
  public showSeconds = true;
  public touchUi = false;
  public enableMeridian = false;
  public stepHour = 1;
  public stepMinute = 1;
  public stepSecond = 1;
  public color: ThemePalette = 'primary';

  public listColors = ['primary', 'accent', 'warn'];

  public stepHours = [1, 2, 3, 4, 5];
  public stepMinutes = [1, 5, 10, 15, 20, 25];
  public stepSeconds = [1, 5, 10, 15, 20, 25];

  
  // Local attributes linked to Form values.
  dateStart: Date;
  dateEnd: Date;
  step: number;
  minElevation: number;
  nbSatellitesToCover: number;
  satConstMode: string = '';
  selectedRegion: RegionModel = CoverageService.DEFAULT_REGION;
  plotMode: string = '';
  cropper: CropperPosition = CoverageService.DEFAULT_CROPPER;

  // Satellite list and subscription
  satelliteList: SatelliteModel[];
  satelliteListSubscription: Subscription;
  selectedSatellite: SatelliteModel;

  // Constellation list and subscription
  constellationList: ConstellationModel[];
  constellationListSubscription: Subscription;
  selectedConstellation: ConstellationModel;

  // The coverage functionnality settings
  //coverageSettings: CoverageSettingsModel;
  coverageSettingsSubscription: Subscription;
  
  // Celestial body subscription
  cbSubscription: Subscription;

  imageURL: any = '';
  croppedImage: any = '';
  isCropperReady: boolean = false; // Flag to know if we can interact with the Image Cropper.

  /**
   * Flag to protect the initialization phase from 
   * the parasitic early imageCropped event raised by the cropper.
   */
  skipFirstImageCroppedEvent: boolean = true;

  constructor(private formBuilder: FormBuilder, private missionService: MissionService, private httpClient: HttpClient) {
    super();
  }

  // Component initialization method.
  ngOnInit() {
    this.initForm();
  }

  // Form initialization code
  initForm() {
    // Update immediately the local satellites list according to the mission service current state.
    this.satelliteList = this.missionService.getSatellites();
    console.log('Satellites: ' + this.satelliteList);

    // Subscribe to the satellites list update events
    this.satelliteListSubscription = this.missionService.satelliteListSubject.subscribe((satList: SatelliteModel[]) => {      
      this.satelliteList = satList;
      console.log('Satellites: ' + this.satelliteList);
    });

    // Update immediately the local constellations list according to the mission service current state.
    this.constellationList = this.missionService.getConstellations();
    console.log('Constellations: ' + this.constellationList);

    // Subscribe to the constellations list update events
    this.constellationListSubscription = this.missionService.constellationListSubject.subscribe((constList: ConstellationModel[]) => {      
      this.constellationList = constList;
      console.log('Constellations: ' + this.constellationList);
    });

    // Update immediately the background map according to current selected Celestial Body.
    this.updateCelestialBodyImage(this.missionService.getCelestialBody());

    // Subscribe to celestial body change events.
    this.cbSubscription = this.missionService.celestialBodySubject.subscribe((cb: string) => {
      this.updateCelestialBodyImage(cb);
    });

    // Update immediately the local coverage settings.
    this.updateLocalCoverageSettings(this.missionService.getCoverageSettings());

    // Add a default initialization on the coverage time settings: use the global time settings.
    this.initCoverageTimeSettingsIfNeeded();

    // Subscribe to coverage settings change events.
    this.coverageSettingsSubscription = this.missionService.coverageSettingsSubject.subscribe((cs: CoverageSettingsModel) => {
      this.updateLocalCoverageSettings(cs);
    })

    this.vtsCoverageVisualizationForm = this.formBuilder.group(
      {
        dateStart:['',Validators.required],
        dateEnd:['',Validators.required],
        step:['',Validators.required],
        minElevation:['',Validators.required],
        nbSatellitesToCover:['',Validators.required],
        satConstMode:[''],
        selectedSatellite:[''],
        selectedConstellation:[''],
        plotMode:['',Validators.required]
      }
    )
  }

  ngOnDestroy() {
    this.coverageSettingsSubscription.unsubscribe();
    this.cbSubscription.unsubscribe();
    this.constellationListSubscription.unsubscribe();
    this.satelliteListSubscription.unsubscribe();
  }

  /**
   * Return the current selected region (default if undefined).
   */
  getSelectedRegion() : RegionModel {
    if (this.selectedRegion !== undefined || this.selectedRegion !== null) {
      return this.selectedRegion;
    } else {
      return CoverageService.DEFAULT_REGION;
    }
  }

  /**
   * Event handler called when an image crop event is raised.
   * @param event The raised crop event.
   */
  imageCropped(event: ImageCroppedEvent) {
    //this.croppedImage = event.base64;
    const msg = "width=" + event.width + "-height=" + event.height + "-x1=" + event.cropperPosition.x1 + "-y1=" + event.cropperPosition.y1 + "-x2=" + event.cropperPosition.x2 + "-y2=" + event.cropperPosition.y2;
    console.log(msg);

    const region: RegionModel = CoverageService.convertCropperPositionToRegionModel(event.cropperPosition);

    // Update the selected region.
    this.selectedRegion = region;

    // Protect the initialization phase from the mission saving due 
    // to the imageCropped event raised at the very beginning of the page loading.
    if (this.skipFirstImageCroppedEvent) {
      this.skipFirstImageCroppedEvent = false;
    } else {
      // Update the global mission state.
      this.updateGlobalCoverageSettings();
    }
  }

  imageLoaded() {
    // Nothing to do yet
  }

  /**
   * This event handler is called when the image cropper is ready to be used.
   */
  cropperReady() {
    // The image cropper is ready.
    this.isCropperReady = true;
    console.log('IMAGE CROPPER IS READY');

    const cs: CoverageSettingsModel = this.missionService.getCoverageSettings();

    // Update immediatly cropper according to the current mission state
    if (cs !== undefined && cs !== null) {
      const region: RegionModel = cs.region;

      // If the mission state defines a coverage region, we then update it.
      if (region !== undefined && region !== null) {
        console.log("Coverage Settings: region=");
        console.log(CoverageService.convertRegionModelToCropperPosition(region));

        // Update the cropper synchronously only for the first time, when the cropper is ready 
        // (at initialization phase).
        this.cropper = CoverageService.convertRegionModelToCropperPosition(region);
      }
    }
  }

  loadImageFailed() {
    // Nothing to do yet
  }

  /**
   * Update the map background according to the celestial body.
   * @param cb The celestial body to update to.
   */
  updateCelestialBodyImage(cb: string) {
    if (cb == undefined || cb == null || cb == '') {
      cb = 'EARTH';
    }    
    this.imageURL = '../assets/images/cb-latlon-images/' + cb + '.png';
  }

  initCoverageTimeSettingsIfNeeded() {

    // Get the global time settings.
    const ts = this.missionService.getTimeSettings();

    // dateStart
      if (this.dateStart == undefined) {
        let ds = new Date();
        // Initialize with the global start date
        if (ts !== undefined && ts != null) {
          const gds = ts.getStartingDate();
          if (gds !== undefined && gds != null) {
            ds = gds;
          }
        }
        this.dateStart = ds;
      }

      // dateEnd
      if (this.dateEnd == undefined) {
        let de = new Date();
        // Initialize with the global end date
        if (ts !== undefined && ts != null) {
          const ges = ts.getEndingDate();
          if (ges !== undefined && ges != null) {
            de = ges;
          }
        }
        this.dateEnd = de;
      }

      // step
      if (this.step == undefined) {
        let s = 60;
        // Initialize with the global step
        if (ts !== undefined && ts != null) {
          const gs = ts.stepTime;
          if (gs !== undefined && gs != null) {
            s = gs;
          }
        }
        this.step = s;
      }
  }

  /**
   * Update the local coverage settings.
   * @param cs The coverage settings to get up-to-date values from.
   */
  updateLocalCoverageSettings(cs: CoverageSettingsModel) {
    // Check coverage settings validity before updating.
    if (cs !== undefined && cs !== null) {
      // dateStart
      let ds = undefined;
      if (cs.dateStart !== undefined && cs.dateStart !== null) {
        ds = cs.dateStart;
      }

      // dateEnd
      let de = undefined;
      if (cs.dateEnd !== undefined && cs.dateEnd !== null) {
        de = cs.dateEnd;
      }

      // step
      let s = undefined;
      if (cs.step != undefined && cs.step != null) {
        s = cs.step;
      }

      // minElevation
      let me = 0;
      if (cs.minElevation != undefined && cs.minElevation != null) {
        me = cs.minElevation;
      }

      // nbSatsToCover      
      let nstc = 1;
      if (cs.nbSatsToCover !== undefined && cs.nbSatsToCover !== null) {
        nstc = cs.nbSatsToCover;
      }

      // Satellite/Constellation mode
      let scm = '';
      if (cs.satConstMode !== undefined && cs.satConstMode != null) {
        scm = cs.satConstMode;
      }

      // Satellite or constellation name
      let scn = '';
      if (cs.satConstName !== undefined && cs.satConstName !== null) {
        scn = cs.satConstName;
      }

      // Region
      let rm = new RegionModel(-90.0, 90.0, -180.0, 180.0);
      if (cs.region !== undefined && cs.region !== null) {
        rm = cs.region;
      }

      // Plot mode
      let pm = cs.plotMode;

      // Update local values linked to Form values.
      this.dateStart = ds;
      this.dateEnd = de;
      this.step = s;
      this.minElevation = me;
      this.nbSatellitesToCover = nstc;
      this.satConstMode = scm;
      if (scn !== '') {
        if (this.satConstMode == 'satellite') {
            this.selectedSatellite = this.missionService.getSatelliteByName(scn);
        } else if (this.satConstMode == 'constellation') {
            this.selectedConstellation = this.missionService.getConstellationByName(scn);
        }
      }
      this.plotMode = pm;

    } else {
      console.error('Coverage settings, to update from, are undefined or null !');
    }
  }

  /**
   * Update the mission global coverage settings.
   */
  updateGlobalCoverageSettings() {

    let scn = '';
    if (this.satConstMode !== undefined && this.satConstMode !== null) {
      if (this.satConstMode == 'satellite') {
        const s : SatelliteModel = this.selectedSatellite;
        if (s !== undefined && s !== null) {
          scn = s.name;
        }
      } else if (this.satConstMode == 'constellation') {
        const c : ConstellationModel = this.selectedConstellation;
        if (c !== undefined && c !== null) {
          scn = c.name;
        }
      }
    }    
        
    // Create updated instance of the coverage settings.
    const cs : CoverageSettingsModel = 
      new CoverageSettingsModel(
        this.dateStart, 
        this.dateEnd, 
        this.step,
        this.minElevation, 
        this.nbSatellitesToCover, 
        this.satConstMode,
        scn, 
        this.selectedRegion, 
        this.plotMode);

    this.missionService.updateCoverageSettings(cs);

    // Update the stored mission data set.
    this.missionService.saveCurrentMissionData().then(
      (status: Map<string, string>) => {

          // All messages land here (even errors).
          this.updateMessages(status);
        }
    );
  }

  /**
   * Update the local component values with the form content.
   */
  updateLocalValuesWithForm() {
    // Get the form values.
    const coverageFormValue = this.vtsCoverageVisualizationForm.value;
    this.dateStart = coverageFormValue['dateStart'];
    this.dateEnd = coverageFormValue['dateEnd'];
    this.step = coverageFormValue['step'];

    this.minElevation = coverageFormValue['minElevation'];
    this.nbSatellitesToCover = coverageFormValue['nbSatellitesToCover'];

    this.satConstMode = coverageFormValue['satConstMode'];
    this.selectedSatellite = coverageFormValue['selectedSatellite'];
    this.selectedConstellation = coverageFormValue['selectedConstellation'];
    // The cropper is updated automatically in the 'imageCropped' event handler.
    this.plotMode = coverageFormValue['plotMode'];
  }

  /**
   * Check if correct values are available to launch a VTS visualization
   * @return true if the data set is valid, false otherwise.
   */ 
  isMissionDataSetValid() {
    let valid: boolean = true;
    let messages: string[] = [];

    const cb = this.missionService.getCelestialBody();

    // Update local component values with the form content.
    this.updateLocalValuesWithForm();
    
    // Check if the celestial body is set.
    if (cb == undefined || cb == '') {
      messages.push("The celestial body is not defined !");
      valid = false;
    }

    // Check date range validity
    if (this.dateStart == undefined || this.dateEnd == undefined) {
      messages.push("Time settings are not correctly defined (one date is undefined) !");
      valid = false;
    } else if (this.dateStart.toString() == "" || this.dateEnd.toString() == "") {
      messages.push("Time settings are not correctly defined (one date is undefined) !");
      valid = false;
    } else if (new Date(this.dateStart).getTime() >= new Date(this.dateEnd).getTime()) {
      messages.push("Time settings are not correctly defined (start date is equal or after end date) !");
      valid = false;
    }

    // Check the time step value.
    if (this.step == undefined || this.step == null || this.step.toString() == "") {
      messages.push("The time step is undefined !");
      valid = false;
    }

    // Check the minimum elevation value.
    if (this.minElevation == undefined || this.minElevation == null || this.minElevation.toString() == "") {
      messages.push("The minimum elevation is undefined !");
      valid = false;
    }

    // Check the number of satellites value.
    if (this.nbSatellitesToCover == undefined || this.nbSatellitesToCover == null || this.nbSatellitesToCover.toString() == "") {
      messages.push("The number of satellites needed to cover a point is undefined !");
      valid = false;
    }

    // Check if Satellites list or constellations list contains at least one selected item.
    if (this.satConstMode == undefined || this.satConstMode == "") {
      messages.push("Satellite or constellation mode is undefined !");
      valid = false;
    }
    else if ((this.selectedSatellite == undefined || this.selectedSatellite == null) && 
             (this.selectedConstellation == undefined || this.selectedConstellation == null)) {
      messages.push("Neither a satellite nor a constellation is selected !");
      valid = false;
    }

    if (this.plotMode == undefined || this.plotMode == "") {
      messages.push("The plot mode is undefined !");
      valid = false;
    }

    var invalidInputParameters = '';
    // Concatenate messages, one by line.
    for (let msg in messages) {
      invalidInputParameters = invalidInputParameters.concat(messages[msg]);
      invalidInputParameters = invalidInputParameters.concat('\n');
    }

    this.setErrorMessage(invalidInputParameters);

    return valid;
  } 

  /**
   * Check the mission data set and launch VTS to visualize it.
   */
  launchVTSVisualization() {
    console.log('launchVTSVisualization');

    // Check the mission data set before launching VTS.
    if (!this.isMissionDataSetValid()) {
      return;
    }

    // Build the HTTP Request intended to the REST API for the Coverage functionnality.
    let req = this.missionService.buildJSatOrbRESTRequestWithCoverageSettings();

    console.log("REQUEST=" + req);

    const httpOptions = {
      responseType: 'blob' as 'json',
      contentType: 'application/vnd+cssi.vtsproject+zip' as const,
      observe: 'response' as const
    };
  
    this.httpClient.post('http://localhost:8000/vts', req, httpOptions).subscribe((data: any) => {

      // The HTTP reponse headers
      var headers = data.headers;

      // Content-type should be 'application/vnd+cssi.vtsproject+zip'
      var contentType = headers.get('Content-Type')
      var respType = 'application/vnd+cssi.vtsproject+zip';

      if (contentType !== null) {
        respType = contentType;
      }

      // String format: "attachment; filename='vts-<mission-name>-content.vz'"
      var contentDisposition = headers.get('Content-Disposition')
      var filename = "vts-content.vz"
      var mission = 'Unknown';

      const FILENAME = "filename='";
      if (contentDisposition !== null && contentDisposition.match("attachment; filename='.+'")) {
        // Extract the received data recommended filename.
        filename = contentDisposition.substring(contentDisposition.indexOf(FILENAME) + FILENAME.length, contentDisposition.length - 1);

        // Get the mission name (not used yet).
        if (filename.match("vts-.+-content.vz")) {
          const VTS = 'vts-';
          const CONTENT = '-content.vz';
          // Extract the mission name.
          mission = filename.substring(filename.indexOf(VTS) + VTS.length, filename.length - CONTENT.length);
        }
      }

      // The HTTP response body (blob containing the VTS compressed data)
      var respBody = data.body

      var blob = new Blob([respBody], { type: respType });
      var url = window.URL.createObjectURL(blob);

      // Generate an HTML link in memory on which we trigger the click event.
      var link = document.createElement('a');
      link.href = url;
      link.download = filename;
      link.type = respType;
      link.rel = 'stylesheet';
      link.click();
    
    });
  }
}

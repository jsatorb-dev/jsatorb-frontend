import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConstellationWizardFormComponent } from './constellation-wizard-form.component';

describe('ConstellationWizardFormComponent', () => {
  let component: ConstellationWizardFormComponent;
  let fixture: ComponentFixture<ConstellationWizardFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConstellationWizardFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConstellationWizardFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { MissionService } from 'src/app/Services/mission/mission.service';
import { Router } from '@angular/router';
import { BaseDisplayMessageComponent } from '../base-display-message/base-display-message.component';
import { ConstellationModel } from 'src/app/Models/Constellation.model';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { SatelliteModel } from 'src/app/Models/Satellite.model';
import { KeplerianSatModel } from 'src/app/Models/KeplerianSat.model';

@Component({
  selector: 'app-constellation-wizard-form',
  templateUrl: './constellation-wizard-form.component.html',
  styleUrls: ['./constellation-wizard-form.component.scss']
})
export class ConstellationWizardFormComponent extends BaseDisplayMessageComponent  implements OnInit {

  constellationWizardForm: FormGroup; // The constellation wizard Form group
  
  // Constellation's name
  consName: string = ''; 

  sma: number = 6471000;
  inclination: number = 0;
  firstRAAN: number = 0;
  walkerT: number = 6;
  walkerP: number = 2;
  walkerF: number = 1;
  constellationColorValue: string = undefined;

  // List of the generated satellites for the constellation.
  satellitesInConsList: string[] = []; 

  routeSubscription: Subscription;

  constructor(private formBuilder: FormBuilder, private missionService: MissionService, private router: Router, private httpClient: HttpClient) {
    super();
  }

  ngOnInit() {
    this.initForm();
  }

  // Form initialization code
  initForm() {
    this.constellationWizardForm = this.formBuilder.group(
      {
        consName:['',Validators.required],
        sma:['',Validators.required],
        inclination:['',Validators.required],
        firstRAAN:['',Validators.required],
        walkerT:['',Validators.required],
        walkerP:['',Validators.required],
        walkerF:['',Validators.required],
        constellationColorValue:['']
      }
    )
  }

  /**
   * Store the constellation in the mission data set.
   */
  public onCreateConstellation() {
    // Step 1: Get the form values.
    const constellationWizardFormValue = this.constellationWizardForm.value;
    this.sma = constellationWizardFormValue['sma'];
    this.inclination = constellationWizardFormValue['inclination'];
    this.firstRAAN = constellationWizardFormValue['firstRAAN'];
    this.walkerT = constellationWizardFormValue['walkerT'];
    this.walkerP = constellationWizardFormValue['walkerP'];
    this.walkerF = constellationWizardFormValue['walkerF'];
    this.constellationColorValue = constellationWizardFormValue['constellationColorValue'];

    // Step 2: Call the REST API to create the satellites.
    // Build the HTTP Request intended to the REST API (route: dateconversion).
    let req = {
      'header': {
        'name': this.consName,
        'sma': this.sma,
        'inc': this.inclination,
        'firstraan': this.firstRAAN,
        't': this.walkerT,
        'p': this.walkerP,
        'f': this.walkerF
      }
    };

    // Add the color if it has been defined.
    if (this.constellationColorValue !== undefined) {
      req['header']['color'] = this.constellationColorValue;
    }

    /**
     * Post the request to the REST API (route: constellationgenerator) and get the resulting value.
     * Request format: see the map above; all fields are stored in a 'header'.
     * Response format: a map containing a list of satellites. 
     */ 
    this.httpClient.post('http://localhost:8000/constellationgenerator', req)
    .toPromise()
    .then((res: any) => {
      const status = res['status'];
      const message = res['message'];
      const data = res['data'];

      if (status == 'SUCCESS') {
        this.setInfoMessage('Request successfully sent to the JSatOrb REST API.');

        // Step 3: Get the generated satellites from the REST API response.
        const jsonSatellites = data;

        var satellitesInConsList = [];
        var satellites : KeplerianSatModel[] = [];
        jsonSatellites.forEach( (sat) => {
          satellitesInConsList.push(sat['name']);
          const satModel = new KeplerianSatModel(
            sat['name'], 
            sat['type'], 
            sat['sma'], 
            sat['ecc'], 
            sat['inc'], 
            sat['pa'], 
            sat['raan'], 
            sat['meanAnomaly'], 
            sat['color']);
          satellites.push(satModel);
        });

        // Step 4: Add the satellites to the mission.
        this.missionService.addConstellation(new ConstellationModel(this.consName, satellitesInConsList));
        this.missionService.addKeplerianSatellites(satellites);

        // Step 5: Update the stored mission data set.
        this.missionService.saveCurrentMissionData().then(
          (status: Map<string, string>) => {

              // All messages land here (even errors).
              this.updateMessages(status);

              if (status.get('status') == 'SUCCESS') {
                setTimeout(
                  () => {
                  // Now the object is created, go to its own URL, 
                  // to enable correct management of a possible new object creation.
                  this.router.navigate(['/constellation/' + this.consName]);
                }, 
                  2000
                );
              }
            }
        );
      } else {
        this.setErrorMessage("An error occured whiele sending the request to the REST API: " + message);
      }
    },
    (error: ProgressEvent) => {
      const msg = error['message'];
      console.error(msg);
      this.setErrorMessage(msg);
    });
  }
}

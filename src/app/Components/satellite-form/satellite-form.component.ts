import {Component, OnDestroy, OnInit} from '@angular/core';
import {MissionService} from '../../Services/mission/mission.service';
import {SatelliteModel} from '../../Models/Satellite.model';
import {TleSatModel} from '../../Models/TleSat.model';
import {KeplerianSatModel} from '../../Models/KeplerianSat.model';
import {CartesianSatModel} from '../../Models/CartesianSat.model';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import { BaseDisplayMessageComponent } from '../base-display-message/base-display-message.component';

@Component({
  selector: 'app-satellite-form',
  templateUrl: './satellite-form.component.html',
  styleUrls: ['./satellite-form.component.scss']
})
export class SatelliteFormComponent extends BaseDisplayMessageComponent implements OnInit, OnDestroy {

  routeSubscription: Subscription;
  satelliteListSubscription : Subscription;

  satType = '';
  colorValue = '#f44295';
  satName = '';
  tleLine1 = '';
  tleLine2 = '';
  sma: number = null;
  ecc: number = null;
  inc: number = null;
  omega: number = null;
  raan: number = null;
  meanAnomaly: number = null;
  x: number = null;
  y: number = null;
  z: number = null;
  vx: number = null;
  vy: number = null;
  vz: number = null;

  constructor(private missionService: MissionService, private router: Router, private route: ActivatedRoute) {
    super();
  }

  ngOnInit() {
    this.routeSubscription = this.route.params.subscribe(
      params => {
        const satName = params['name'];
        const sat = this.missionService.getSatelliteByName(satName);
        if (sat !== undefined) {
          this.satName = sat.name;
          this.satType = sat.type;
          this.colorValue = sat.color;
          switch (sat.type) {
            case 'tle':
              const tleSat = sat as TleSatModel;
              this.tleLine1 = tleSat.line1;
              this.tleLine2 = tleSat.line2;
              break;
            case 'keplerian':
              const keplerianSat = sat as KeplerianSatModel;
              this.sma = keplerianSat.sma;
              this.ecc = keplerianSat.ecc;
              this.inc = keplerianSat.inc;
              this.omega = keplerianSat.pa;
              this.raan = keplerianSat.raan;
              this.meanAnomaly = keplerianSat.meanAnomaly;
              break;
            case 'cartesian':
              const cartesianSat = sat as CartesianSatModel;
              this.x = cartesianSat.x;
              this.y = cartesianSat.y;
              this.z = cartesianSat.z;
              this.vx = cartesianSat.vx;
              this.vy = cartesianSat.vy;
              this.vz = cartesianSat.vz;
          }
        }
      });

    // Subscribe to the satellites list update events
    this.satelliteListSubscription = this.missionService.satelliteListSubject.subscribe((satList: SatelliteModel[]) => {

      /**
       * If the satellite currently edited is no more to be found in the mission, it means it just has been removed.
       */
      const satname = this.satName;
      if (satname !== undefined && satname !== null && satname !== "") {

        // Look for its name in the mission satellites list.
        const foundSat = this.missionService.getSatellites().find( satellite => satellite.name === satname );

        // If the satellite has not been found, re-route the current content to the 'new satellite' form.
        if (foundSat === undefined || foundSat === null) {
          setTimeout(
            () => {
              this.router.navigate(['/satellite']);
            }, 
            2000
          );
        }
      }
    });
  }

  ngOnDestroy() {
    this.routeSubscription.unsubscribe();
    this.satelliteListSubscription.unsubscribe();
  }

  public onSavingSat() {
    let satForm: SatelliteModel;
    switch (this.satType) {
      case 'tle':
        satForm = new TleSatModel(this.satName, this.satType, this.tleLine1, this.tleLine2, this.colorValue);
        break;
      case 'keplerian':
        satForm = new KeplerianSatModel(this.satName, this.satType, this.sma, this.ecc, this.inc, this.omega, this.raan, this.meanAnomaly,
          this.colorValue);
        break;
      case 'cartesian':
        satForm = new CartesianSatModel(this.satName, this.satType, this.x, this.y, this.z, this.vx, this.vy, this.vz, this.colorValue);
        break;
    }
    if (satForm !== undefined) {
      this.missionService.addSatellite(satForm);

      // Update the stored mission data set.
      this.missionService.saveCurrentMissionData().then(
        (status: Map<string, string>) => {

            // All messages land here (even errors).
            this.updateMessages(status);

            if (status.get('status') == 'SUCCESS') {
              setTimeout(
                () => {
                // Now the object is created, go to its own URL, 
                // to enable correct management of a possible new object creation.
                this.router.navigate(['/satellite/' + this.satName]);
                }, 
                2000
              );
            }
          }
      );
    }
  }
}

import {Component, OnDestroy, OnInit} from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import {Observable, Subscription} from 'rxjs';
import { map } from 'rxjs/operators';
import {MissionService} from '../../Services/mission/mission.service';
import {SatelliteModel} from '../../Models/Satellite.model';
import {GroundStationModel} from '../../Models/GroundStation.model';
import {TimeSettingsModel} from '../../Models/TimeSettings.model';
import {CzmlDocumentModel} from '../../Models/CzmlDocument.model';
import {CzmlService} from '../../Services/czml/czml.service';
import {HttpClient} from '@angular/common/http';
import {CzmlSatelliteModel} from '../../Models/CzmlSatellite.model';
import {CzmlVisibilityModel} from '../../Models/CzmlVisibility.model';
import { ThemePalette } from '@angular/material/core';
import { ConstellationModel } from 'src/app/Models/Constellation.model';
import { ConstantsService } from 'src/app/Services/mission/constants.service';

@Component({
  selector: 'app-responsive-navigation',
  templateUrl: './responsive-navigation.component.html',
  styleUrls: ['./responsive-navigation.component.scss'],
})
export class ResponsiveNavigationComponent implements OnDestroy, OnInit {

  duration: number;
  step: number;
  dateStart: Date;
  dateEnd: Date;

  satelliteList: SatelliteModel[];
  satelliteListSubscription: Subscription;

  constellationList: ConstellationModel[];
  constellationListSubscription: Subscription;

  groundStationList: GroundStationModel[];
  groundStationListSubscription: Subscription;

  timeSettingsSubscription: Subscription;

  // Current mission name
  currentMission: string;
  currentMissionSubscription: Subscription;

  // List of available celestial bodies.
  celestialBodies: string[];

  // The currently selected celestial body.
  selectedCelestialBody: string;
  // Subscription to celestial body changes (when loading a dataset for instance).
  celestialBodySubscription: Subscription;

  /** DateTime picker variables */
  public disabled = false;
  public showSpinners = true;
  public showSeconds = true;
  public touchUi = false;
  public enableMeridian = false;
  public stepHour = 1;
  public stepMinute = 1;
  public stepSecond = 1;
  public color: ThemePalette = 'primary';

  public listColors = ['primary', 'accent', 'warn'];

  public stepHours = [1, 2, 3, 4, 5];
  public stepMinutes = [1, 5, 10, 15, 20, 25];
  public stepSeconds = [1, 5, 10, 15, 20, 25];

  /**
   * Variable du formulaire
   */

  /**
   * Manage responsive menu
   */
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(private breakpointObserver: BreakpointObserver, private missionService: MissionService, private czmlService: CzmlService,
              private httpClient: HttpClient) {}
    /**
     * Abonnement aux liste dans le service Mission pour pouvoir observer les changements
     */

  ngOnInit() {
    // Get list of celestial bodies.
    this.celestialBodies = ConstantsService.CELESTIAL_BODIES;

    // Set default mission
    this.missionService.setDefaultCurrentMission();

    // Subscribe to the satellites list update events
    this.satelliteListSubscription = this.missionService.satelliteListSubject.subscribe((satList: SatelliteModel[]) => {      
      this.satelliteList = satList;
    });

    // Subscribe to the constellations list update events
    this.constellationListSubscription = this.missionService.constellationListSubject.subscribe((constList: ConstellationModel[]) => {      
      this.constellationList = constList;
    });

    // Subscribe to the ground stations list update events
    this.groundStationListSubscription = this.missionService.groundStationListSubject.subscribe((gsList: GroundStationModel[]) => {
      this.groundStationList = gsList;
    });

    // Subscribe to the current mission events
    this.currentMissionSubscription = this.missionService.currentMissionSubject.subscribe(currentMission => this.currentMission = currentMission);

    // Subscribe to the time settings events
    this.timeSettingsSubscription = this.missionService.timeSettingsSubject.subscribe((time) => {
      if (time !== undefined) {
        this.duration = time.duration;
        this.step = time.stepTime;
        this.dateStart = time.dateStart;
        this.dateEnd = time.dateEnd;
      }      
    });

    // Subscribe to the celestial body update events.
    this.celestialBodySubscription = this.missionService.celestialBodySubject.subscribe(celestialBody => this.selectedCelestialBody = celestialBody);

    // Force the different subject's update notifications
    this.missionService.emitSatellites();
    this.missionService.emitConstellations();
    this.missionService.emitGroundStations();
    this.missionService.emitCurrentMission();
    this.missionService.emitTimeSettings();
    this.missionService.emitCelestialBody();

    // Force the mission data set list update.
    this.missionService.emitMissionDataSetList();
  }

  ngOnDestroy() {
    this.celestialBodySubscription.unsubscribe();
    this.timeSettingsSubscription.unsubscribe();
    this.currentMissionSubscription.unsubscribe();
    this.groundStationListSubscription.unsubscribe();
    this.constellationListSubscription.unsubscribe();
    this.satelliteListSubscription.unsubscribe();
  }

  removeSat(sat: SatelliteModel) {
    this.missionService.removeSat(sat);
    //this.czmlService.removeCzmlSatellite(sat.name);
  }

  /**
   * Delete a constellation.
   * @param cons The constellation to delete.
   */
  removeCons(cons: ConstellationModel) {
    this.missionService.removeConstellation(cons);
  }

  removeGs(gs: GroundStationModel) {
    this.missionService.removeGroundStation(gs);
    //this.czmlService.removeGroundStation(gs.name);
  }

  /**
   * Update the current time settings according to the user input.
   */
  saveTimeSettings() {
    try {
      var dateStartSelected : any = '';
      var dateEndSelected : any = '';

      if (this.dateStart !== null && this.dateStart !== undefined) {
        console.log("Selected Starting date =" + this.dateStart.toString());
        dateStartSelected = ConstantsService.formatDateISO8601(this.dateStart);
      } else {
        console.log("Selected Starting date =" + this.dateStart);
      }
      if (this.dateEnd !== null && this.dateEnd !== undefined) {
        console.log("Selected Ending date =" + dateEndSelected.toString());
        dateEndSelected = ConstantsService.formatDateISO8601(this.dateEnd);
      } else {
        console.log("Selected Ending date =" + this.dateEnd);
      }
      const newTimeSetting: TimeSettingsModel = new TimeSettingsModel(dateStartSelected, dateEndSelected, this.step);
      // const czmlDoc =  new CzmlDocumentModel(newTimeSetting.getStartingDate(),
      //   newTimeSetting.getEndingDate(), Number(newTimeSetting.stepTime));
      this.missionService.updateTimeSettings(newTimeSetting);
      // this.czmlService.updateDocument(czmlDoc);

    } catch (e) {
    }

    // Store the new mission data set state.
    this.missionService.saveCurrentMissionData().then(
      (status: Map<string, string>) => {

          // All messages land here (even errors).
          console.log(status);
        }
    );
  }

  /**
   * Celestial body new selection event handler.
   * @param newValue The newly selected value.
   */
  onSelectCelestialBody(newValue) {
    this.selectedCelestialBody = newValue;
    
    // Save the new mission state.
    this.saveCelestialBody();
  }

  /**
   * Update the current celestial body according to the user input.
   */
  saveCelestialBody() {
    this.missionService.updateCelestialBody(this.selectedCelestialBody);
    console.log(this.missionService.celestialBody);

    // Store the new mission data set state.
    this.missionService.saveCurrentMissionData().then(
      (status: Map<string, string>) => {

          // All messages land here (even errors).
          console.log(status);
        }
    );
  }

  /**
   * Get the list of all satellites present in the misssion data set.
   */
  public getSatellites() {
    return this.missionService.getSatellites();
  }

  /**
   * Get the list of all TLE satellites present in the misssion data set.
   */
  public getTLESatellites() {
    return this.missionService.getTLESatellites();
  }

  /**
   * Get the list of all NON TLE satellites present in the misssion data set.
   */
  public getNonTLESatellites() {
    return this.missionService.getNonTLESatellites();
  }
}

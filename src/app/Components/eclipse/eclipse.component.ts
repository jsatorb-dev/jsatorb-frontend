import { Component, OnInit } from '@angular/core';
import {SatelliteModel} from '../../Models/Satellite.model';
import {MissionService} from '../../Services/mission/mission.service';
import {HttpClient} from '@angular/common/http';
import {TimeSettingsModel} from '../../Models/TimeSettings.model';
import {TleSatModel} from '../../Models/TleSat.model';
import {KeplerianSatModel} from '../../Models/KeplerianSat.model';
import {CartesianSatModel} from '../../Models/CartesianSat.model';

@Component({
  selector: 'app-eclipse',
  templateUrl: './eclipse.component.html',
  styleUrls: ['./eclipse.component.scss']
})
export class EclipseComponent implements OnInit {

  constructor(private missionService: MissionService, private httpClient: HttpClient) {
  }

  displayGraph = false;
  eclipseData: any;
  selectedSat: SatelliteModel;
  satList: SatelliteModel[];
  timeSettings: TimeSettingsModel;
  ngOnInit() {
    this.missionService.timeSettingsSubject.subscribe((time) => {
      this.timeSettings = time;
    });

    this.missionService.satelliteListSubject.subscribe((satList) => {
      this.satList = satList.filter(sat => sat.type !== 'tle');
    });
    this.missionService.emitSatellites();
    this.missionService.emitTimeSettings();
  }

  onRunningCalculation() {
    let satToSend;
    if (this.selectedSat.type === 'keplerian') {
      satToSend = this.selectedSat as KeplerianSatModel;
    } else if (this.selectedSat.type === 'cartesian') {
      satToSend = this.selectedSat as CartesianSatModel;
    }
    const request = {
      'header': {
        'timeStart': this.timeSettings.getStringFormattedStartDate(),
        'timeEnd': this.timeSettings.getStringFormattedEndDate(),
      },
      'satellite': satToSend
    };
    this.httpClient.post('http://localhost:8000/propagation/eclipses', request).subscribe(
      (eclipse) => {
        this.eclipseData = eclipse;
        this.displayGraph = true;
      });
  }
}

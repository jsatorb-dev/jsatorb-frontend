import { TestBed } from '@angular/core/testing';

import {CzmlService} from './czml.service';

describe('CzmlService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CzmlService = TestBed.get(CzmlService);
    expect(service).toBeTruthy();
  });
});

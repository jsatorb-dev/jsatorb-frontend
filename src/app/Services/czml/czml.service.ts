import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';
import {MissionService} from '../mission/mission.service';
import {TimeSettingsModel} from '../../Models/TimeSettings.model';
import {GroundStationModel} from '../../Models/GroundStation.model';
import {SatelliteModel} from '../../Models/Satellite.model';
import {CzmlGroundStationModel} from '../../Models/CzmlGroundStation.model';
import {CzmlDocumentModel} from '../../Models/CzmlDocument.model';
import {CzmlSatelliteModel} from '../../Models/CzmlSatellite.model';
import {CzmlVisibilityModel} from '../../Models/CzmlVisibility.model';

@Injectable({
  providedIn: 'root'
})
export class CzmlService {
  czmlDataSubject = new Subject<any[]>();
  czmlData =  [];

  constructor() {
  }

  public emitCzmlData() {
    if (this.isCzmlDocumentIsDefined()) {
      this.documentCzmlFirst();
      this.czmlDataSubject.next(this.czmlData.slice());
    }
  }

  private getDocumentTimeInterval(): string {
    let temp: CzmlDocumentModel;
    this.czmlData.forEach((el) => {
      if (el.id === 'document') {
        temp = el as CzmlDocumentModel;
      }
    });
    return temp.clock.interval;
  }

  private updateTimeIntervalOfAllGroundStations(timeInterval: string) {
    this.czmlData = this.czmlData.map((el) => {
      if (el.id.substr(0, 8) === 'Station/') {
        const temp = el as CzmlGroundStationModel;
        temp.availability = timeInterval;
        return temp;
      } else {
        return el;
      }
    });
  }
  private isCzmlDocumentIsDefined(): any {
    let docExist = false;
    this.czmlData.forEach((el) => {
      if (el.id === 'document') {
        docExist = true;
      }
    });
    return docExist;
  }

  public addCzmlSatelliteList(satList: CzmlSatelliteModel[]) {
    satList.forEach((sat) => {
      this.czmlData.push(sat);
    });
    this.emitCzmlData();
  }

  public addCzmlSatellite(sat: CzmlSatelliteModel) {
    this.czmlData.push(sat);
    this.emitCzmlData();
  }

  public addCzmlVisibility(visList: CzmlVisibilityModel[]) {
    visList.forEach((visibility) => {
      this.czmlData.push(visibility);
    });
    this.emitCzmlData();
  }

  public removeCzmlSatellite(satName: string) {
    this.czmlData = this.czmlData.filter((el) => {
      return el.id.search('Satellite/' + satName) < 0;
    });
    this.emitCzmlData();
  }

  private documentCzmlFirst() {
    /**
     * Position the document element as first on the list of data. This is necessity.
     */
    if (this.czmlData[0].id !== 'document') {
      let echangeTemp: any;
      this.czmlData.forEach((entity, index) => {
        if (index === 0) {
          echangeTemp = entity;
        }
        if (entity.id === 'document') {
          this.czmlData[0] = entity;
          this.czmlData[index] = echangeTemp;
        }
      });
    }
  }

  public addGroundStation(name: string, latitude: number, longitude: number, altitude: number): void {
    let itemWithSameIdHasBeenUpdate = false;
    let gs: CzmlGroundStationModel;
    if (this.isCzmlDocumentIsDefined()) {
      gs = new CzmlGroundStationModel(name, longitude, latitude, altitude, this.getDocumentTimeInterval());
    } else {
      gs =  new CzmlGroundStationModel(name, longitude, latitude, altitude);
    }
    this.czmlData = this.czmlData.map((el) => {
      if (el.id === gs.id) {
        itemWithSameIdHasBeenUpdate = true;
        return gs;
      } else {
        return el;
      }
    });
    if (itemWithSameIdHasBeenUpdate === false) {
      this.czmlData.push(gs);
    }
    this.emitCzmlData();
  }

  public removeGroundStation(name: string): void {
    this.czmlData = this.czmlData.filter((el) => el.name !== name);
    this.emitCzmlData();
  }

  public removeAllCzmlVisibilityAndSatellites() {
    this.czmlData = this.czmlData.filter((el) => {
      return el.id.search('Satellite/') < 0;
    });
    this.emitCzmlData();
  }

  public updateDocument(document: CzmlDocumentModel): void {
    let documentHasBeenAss = false;
    this.czmlData = this.czmlData.map((el) => {
      if (el.id === 'document') {
        documentHasBeenAss = true;
        return document;
      } else {
        return el;
      }
    });
    if (!documentHasBeenAss) {
      this.czmlData.push(document);
    }
    this.updateTimeIntervalOfAllGroundStations(document.clock.interval);
    this.emitCzmlData();
  }

}

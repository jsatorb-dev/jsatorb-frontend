import { Injectable } from '@angular/core';
import { GroundStationModel } from '../../Models/GroundStation.model';
import { SatelliteModel } from '../../Models/Satellite.model';
import { Subject } from 'rxjs';
import { TimeSettingsModel } from '../../Models/TimeSettings.model';
import { HttpClient } from '@angular/common/http';
import { ConstellationModel } from 'src/app/Models/Constellation.model';
import { CoverageSettingsModel } from 'src/app/Models/CoverageSettings.model';
import { CoverageService } from 'src/app/Services/mission/coverage.service';
import { ConstantsService } from './constants.service';
import { KeplerianSatModel } from 'src/app/Models/KeplerianSat.model';

@Injectable({
  providedIn: 'root'
})
export class MissionService {
  /**
   * Ground stations list
   */
  groundStationListSubject = new Subject<GroundStationModel[]>();
  groundStationList: GroundStationModel[] = [];

  /**
   * Satellites list
   */
  satelliteListSubject = new Subject<SatelliteModel[]>();
  satelliteList: SatelliteModel[] = [];

  /**
   * Constellations list
   */
  constellationListSubject = new Subject<ConstellationModel[]>();
  constellationList: ConstellationModel[] = [];

  /**
   * Time settings
   */
  timeSettings: TimeSettingsModel;
  timeSettingsSubject = new Subject<TimeSettingsModel>();

  /**
   * Mission's celestial body: EARTH by default.
   */
  celestialBody: string = ConstantsService.DEFAULT_CELESTIAL_BODY;
  celestialBodySubject = new Subject<string>();

  /**
   * Coverage functionnality settings
   */
  coverageSettings: CoverageSettingsModel; // The whole Coverage functionnality settings.
  coverageSettingsSubject = new Subject<CoverageSettingsModel>(); // Subject to manage the coverage settings updates.

  /**
   * General VTS visualization options (excludes the coverage settings).
   * Remark: in the REST API, the 'options' item also contains the coverage settings, but not in the angular code.
   * 
   * The options are an object which each property represents a VTS visualization option.
   * Each option can hav parameters, but only the 'ATTITUDE' option actually has one.
   * The 'COVERAGE' option has got too, but this option, as it is more complex, is managed in a separate variable name coverageSettings'.
   * Only when writing the JSON requests, the options and the coverageSettings are merged into one 'options' property of the request.
   */
  options: object = {};
  optionsSubject = new Subject<object>();

  /**
   * Current mission is a variable that define the current mission data set name.
   * This name is used as an id to store or retrieve a mission data set to or from the back-end.
   */
  currentMission: string = 'Default-mission';
  currentMissionSubject = new Subject<string>();

  /**
   * Mission data set list is a variable that define the list of available mission data set names.
   */
  missionDataSetList: string[] = [];
  missionDataSetListSubject = new Subject<string[]>();

  constructor(private httpClient: HttpClient) {
  }

     /****************************************************************************
   * GROUND STATIONS
   */
  public emitGroundStations() {
    this.groundStationListSubject.next(this.groundStationList.slice());
  }

  public addGroundStation(gs: GroundStationModel): void {
    const existingGs = this.getGroundStationByName(gs.name);
    if (existingGs === undefined) {
      this.groundStationList.push(gs);
    } else {
      this.groundStationList = this.groundStationList.map((el) => {
        if (el.name === gs.name) {
          return gs;
        } else {
          return el;
        }
      });
    }
    this.emitGroundStations();
  }

  /**
   * Setter.
   * @param groundStations The ground stations list.
   */
  public setGroundStations(groundStations) : void {
    this.groundStationList = groundStations;
    this.emitGroundStations();
  }

  public removeGroundStation(gs: GroundStationModel): void {
    this.groundStationList = this.groundStationList.filter((existingStation: GroundStationModel) => existingStation.name !== gs.name);
    this.emitGroundStations();
  }

  /**
   * Get a ground station by its name.
   * @param gsName Name of the ground station to look for.
   * @returns The ground station or undefined if not found.
   */
  public getGroundStationByName(gsName: String): GroundStationModel {
    return this.groundStationList.find( gs => gs.name === gsName );
  }

  /****************************************************************************
   * SATELLITES
   */
  public emitSatellites() {
    this.satelliteListSubject.next(this.satelliteList.slice());
  }

  /**
   * Add a satellite to the mission.
   * @param sat The satellite to add.
   */
  public addSatellite(sat: SatelliteModel): void {
    const existingSat = this.getSatelliteByName(sat.name);
    if (existingSat === undefined) {
      this.satelliteList.push(sat);
    } else {
      this.satelliteList = this.satelliteList.map((el) => {
        if (el.name === sat.name) {
          return sat;
        } else {
          return el;
        }
      });
    }
    this.emitSatellites();
  }

  /**
   * Add an array of Keplerian satellites to the mission.
   * @param sats The satellites to add.
   */
  public addKeplerianSatellites(sats: KeplerianSatModel[]): void {

    sats.forEach( (sat) => {
      const existingSat = this.getSatelliteByName(sat.name);
      if (existingSat === undefined) {
        this.satelliteList.push(sat);
      } else {
        this.satelliteList = this.satelliteList.map((el) => {
          if (el.name === sat.name) {
            return sat;
          } else {
            return el;
          }
        });
      }
    });
    this.emitSatellites();
  }

  /**
   * Setter.
   * @param satellites the satellites list.
   */
  public setSatellites(satellites) : void {
    this.satelliteList = satellites;
    this.emitSatellites();
  }

  /**
   * Getter
   * @returns A list of all satellites.
   */
  public getSatellites(): SatelliteModel[] {
    return this.satelliteList;
  }

  /**
   * Remove a satellite from the mission.
   * It also removes it from all constellations it was referenced in.
   * @param sat The satellite to remove.
   */
  public removeSat(sat: SatelliteModel): void {
    // Remove the satellite from the satellites list.
    this.satelliteList = this.satelliteList.filter((existingSat: SatelliteModel) => existingSat.name !== sat.name);
    this.emitSatellites();

    const satname = sat.name;    
    var consModified = false;
    // Remove the satellite from all the constellations.
    this.constellationList.forEach( (cons) => {
      var nbB = cons.satellites.length;
      cons.satellites = cons.satellites.filter( (conssatname: string) => satname !== conssatname );
      var nbA = cons.satellites.length;

      consModified = consModified || (nbB !== nbA);
    });

    if (consModified) {
      this.emitConstellations();
    }
  }

  /**
   * Get a satellite by its name.
   * @param satName Name of the satellite to look for.
   * @returns The satellite or undefined if not found.
   */
  public getSatelliteByName(satName: String): SatelliteModel {
    return this.satelliteList.find(sat => sat.name === satName );
  }

  /**
   * Get the list of all TLE satellites present in the misssion data set.
   */
  public getTLESatellites() {
    return this.satelliteList.filter( (sat: SatelliteModel) => { return sat.type === 'tle'; });
  }

  /**
   * Get the list of all NON TLE satellites present in the misssion data set.
   */
  public getNonTLESatellites() {
    return this.satelliteList.filter( (sat: SatelliteModel) => { return sat.type !== 'tle'; });
  }

  /****************************************************************************
   * CONSTELLATIONS
   */
  public emitConstellations() {
    this.constellationListSubject.next(this.constellationList.slice());
  }

  public addConstellation(cons: ConstellationModel): void {
    const existingCons = this.getConstellationByName(cons.name);
    if (existingCons === undefined) {
      this.constellationList.push(cons);
    } else {
      this.constellationList = this.constellationList.map((el) => {
        if (el.name === cons.name) {
          return cons;
        } else {
          return el;
        }
      });
    }
    this.emitConstellations();
  }

  /**
   * Setter.
   * @param constellations the constellations list.
   */
  public setConstellations(constellations) : void {
    this.constellationList = constellations;
    this.emitConstellations();
  }

  /**
   * Getter
   * @returns A list of all constellations.
   */
  public getConstellations(): ConstellationModel[] {
    return this.constellationList;
  }

  public removeConstellation(cons: ConstellationModel): void {
    this.constellationList = this.constellationList.filter((existingCons: ConstellationModel) => existingCons.name !== cons.name);
    this.emitConstellations();
  }

  /**
   * Get a constellation by its name.
   * @param consName Name of the constellation to look for.
   * @returns The constellation or undefined if not found.
   */
  public getConstellationByName(consName: String): ConstellationModel {
    return this.constellationList.find(cons => cons.name === consName );
  }
  
  /****************************************************************************
   * TIME SETTINGS
   */
  public emitTimeSettings() {
    const temp: TimeSettingsModel = this.timeSettings;
    this.timeSettingsSubject.next(temp);
  }

  public updateTimeSettings(t: TimeSettingsModel) {
    this.timeSettings = t;
    this.emitTimeSettings();
    console.log("Selected date - start =" + t.dateStart.toString());
    console.log("Selected date - end =" + t.dateEnd.toString());
    console.log("Step =" + t.stepTime);
  }

  public getTimeSettings() {
    return this.timeSettings;
  }

  /****************************************************************************
   * CELESTIAL BODY
   */
  public emitCelestialBody() {
    const temp: string = this.celestialBody;
    this.celestialBodySubject.next(temp);
  }

  public updateCelestialBody(cb: string) {
    this.celestialBody = cb;
    this.emitCelestialBody();
  } 

  public getCelestialBody() {
    if (this.celestialBody == undefined) {
      this.celestialBody = ConstantsService.DEFAULT_CELESTIAL_BODY;
    }
    return this.celestialBody;
  }

  /****************************************************************************
   * COVERAGE SETTINGS
   */
  public emitCoverageSettings() {
    const temp: CoverageSettingsModel = this.coverageSettings;
    this.coverageSettingsSubject.next(temp);
  }

  public updateCoverageSettings(cs: CoverageSettingsModel) {
    this.coverageSettings = cs;
    this.emitCoverageSettings();
  } 

  getCoverageSettings(): CoverageSettingsModel {
    return this.coverageSettings;
  }

  /****************************************************************************
   * OPTIONS
   */
  public emitOptions() {
    const temp: object = this.options;
    this.optionsSubject.next(temp);
  }

  public updateOptions(options: object) {
    this.options = options;
    this.emitOptions();
  } 

  public getOptions() {
    if (this.options == undefined) {
      this.options = [];
    }
    return this.options;
  }

  /****************************************************************************
   * CURRENT MISSION
   */
  /**
   * Emit the current mission name update event to the subject subscribers.
   */
  public emitCurrentMission() {
    const temp: string = this.currentMission;
    this.currentMissionSubject.next(temp);
  }

  /**
   * Getter for the current mission name.
   */
  public getCurrentMission(): string {
    return this.currentMission;
  }

  /**
   * Setter for the current mission name.
   */
  public setCurrentMission(missionName): void {
    this.currentMission = missionName;
    this.emitCurrentMission();
  }

  /**
   * Set the default mission as current mission name.
   */
  public setDefaultCurrentMission(): void {
    this.setCurrentMission('Default-mission');
  }  

  /**
   * Emit the mission data set list update event to the subject subscribers.
   */
  public emitMissionDataSetList() {
    const temp:string[] = this.missionDataSetList;
    this.missionDataSetListSubject.next(temp);
  }

  /**
   * Setter for the mission data set names list.
   */
  public setMissionDataSetList(missionNames): void {
    this.missionDataSetList = missionNames;
    this.emitMissionDataSetList();
  }


  /****************************************************************************
   * MISSION DATA SETS MANAGEMENTS
   */

  /**
   * Store current mission data set to back-end.
   * @param name Mission name
   * @returns An error message if a problem occurs (empty if success)
   */
  public saveCurrentMissionData(): Promise<Map<string,string>> {
    return this.saveMissionData(this.currentMission);
  }

  /**
   * Store mission data set to back-end.
   * @param name Mission name
   * @returns A map containing key/values: status={'SUCCESS' or 'FAIL} and message='<operation message>'
   */
  public saveMissionData(missionName: string): Promise<Map<string, string>> {
    let result: Map<string, string> = new Map();

    const ts = this.timeSettings;

    // Prevent undefined time settings to raise errors
    let start = undefined;    
    let end = undefined;    
    if (ts !== undefined) {
      if (ts.dateStart !== undefined) {
        start = ts.getStringFormattedStartDate();
      }
      if (ts.dateEnd !== undefined) {
        end = ts.getStringFormattedEndDate();
      }
    }

    let stepTime = undefined;
    if (ts !== undefined) {
      if (ts.stepTime !== undefined) {
        stepTime = ts.stepTime;
      }
    }

    // Build the REST options structure by merging the MissionService 'options' containing all options except those of the Coverage functionnality),
    // with the Coverage settings.

    // Get the coverage settings' options part.
    const coverageSettingsOptions = CoverageService.getCoverageSettingsAsJSONOptions(this.getCoverageSettings());

    const mergedOptions = { ...this.options, ...coverageSettingsOptions};

    let req = {
      'header': {
        'mission': missionName,
        'celestialBody': this.celestialBody,
        'timeStart': start,
        'timeEnd': end,
        'step': stepTime
      },
      'satellites': this.satelliteList,
      'constellations': this.constellationList,
      'groundStations': this.groundStationList,
      'options': mergedOptions
    };

    return this.httpClient.post('http://localhost:8000/missiondata/' + missionName, req)
      .toPromise()
      .then(
      (res: any) => {
        const status = res['status'];
        const message = res['message'];
  
        result.set('status', status);

        if (status == 'SUCCESS') {
          console.log("Mission data has been stored");
          // Update the current mission
          this.setCurrentMission(missionName);
          // Update the mission data set list.
          this.updateMissionDataSetList();   

          const msg = 'JSatOrb API Request succeeded';
          result.set('message', msg);
          console.log(msg);
          return result;     
        } else {
          const msg =  "Mission data has not been stored: " + message;
          result.set('message', msg);
          console.error(msg);
          return result;
        }
      },
      (error: ProgressEvent) => {
        let result: Map<string, string> = new Map();
        const msg = error['message'];
        result.set('status', 'FAIL');
        result.set('message', msg);
        console.error(msg);
        return result;
      }
    );
  }

  /**
   * Load mission data set from back-end.
   * @param missionName Mission name
   * @returns A map containing key/values: status={'SUCCESS' or 'FAIL} and message='<operation message>'
   */
  public loadMissionData(missionName: String): Promise<Map<string, string>> {
    return this.httpClient.get('http://localhost:8000/missiondata/' + missionName)
    .toPromise()
    .then(
    (res: any) => {
      let result: Map<string, string> = new Map();

      const status = res['status'];
      const message = res['message'];
      const data = res['data'];

      result.set('status', status);

      if (status == 'SUCCESS') {
        console.log("Mission data has been received");
        console.log("Content is:");
        console.log(data);

        const header = data['header'];
        const mission = header['mission'];
        const celestialbody = header['celestialBody'];
        const stepTime = header['step'];
        const dateStart = header['timeStart'];
        const dateEnd = header['timeEnd'];

        // ------ Update the GUI mission data set attributes.

        // Celestial body
        if (celestialbody !== undefined) {
          this.updateCelestialBody(celestialbody);
        } else {
          this.updateCelestialBody(ConstantsService.DEFAULT_CELESTIAL_BODY);
        }

        // Time settings
        const timeSettings = new TimeSettingsModel(dateStart, dateEnd, stepTime);
        this.updateTimeSettings(timeSettings);

        // Satellites
        const satelliteList = data['satellites'];
        if (satelliteList !== undefined) {
          this.setSatellites(satelliteList);
        }

        // Constellations
        const constellationList = data['constellations'];
        if (constellationList !== undefined) {
          this.setConstellations(constellationList);
        }

        // Ground stations
        const groundStationList = data['groundStations']
        if (groundStationList !== undefined) {
          this.setGroundStations(groundStationList);
        }

        // Options
        const options = data['options'];
        if (options !== undefined) {

          // Update separately the Coverage settings and the other options.
          var cs: CoverageSettingsModel = undefined;
          var otherOptions = {};

          // Process the JSON 'options'.
          Object.keys(options).forEach(
            (key) => {
              if (key == 'COVERAGE') {
                // Get back the different coverage settings.
                cs = CoverageService.getCoverageSettingsFromJSONOptions(options[key]);
              } else {
                // Copy the sub-object into the "other options".
                otherOptions[key] = options[key];
              }
            }
          );

          // Update the coverage settings.
          this.updateCoverageSettings(cs);

          // Update the "other options".
          this.updateOptions(otherOptions);
        }

        // Current mission
        if (mission !== undefined) {
          this.setCurrentMission(mission);
        } else {
          this.setCurrentMission(missionName);
        }
        const msg = 'Mission data has been correctly loaded';
        result.set('message', msg);
        console.log(msg);
        return result;
      } else {
        const msg = 'Mission data has not been loaded: ' + message;
        result.set('status', 'FAIL');
        result.set('message', msg);
        console.error(msg);
        return result;
      }
    },
    (error: ProgressEvent) => {
      let result: Map<string, string> = new Map();
      
      const msg = error['message'];
      result.set('status', 'FAIL');
      result.set('message', msg);
      console.error(msg);
      return result;
    }
  );
  }

    /**
   * Delete a mission data set stored on the back-end.
   * @param name Mission name
   * @returns A map containing key/values: status={'SUCCESS' or 'FAIL} and message='<operation message>'
   */
  public deleteMissionData(missionName: string): Promise<Map<string, string>> {
    let result: Map<string, string> = new Map();

    return this.httpClient.delete('http://localhost:8000/missiondata/' + missionName)
      .toPromise()
      .then(
      (res: any) => {
        const status = res['status'];
        const message = res['message'];
  
        result.set('status', status);

        if (status == 'SUCCESS') {
          console.log("Mission data has been deleted");
          // // Update the current mission
          // this.setCurrentMission(missionName);
          // Update the mission data set list.
          this.updateMissionDataSetList();   

          const msg = 'JSatOrb API Request succeeded';
          result.set('message', msg);
          console.log(msg);
          return result;     
        } else {
          const msg =  "Mission data has not been deleted: " + message;
          result.set('message', msg);
          console.error(msg);
          return result;
        }
      },
      (error: ProgressEvent) => {
        let result: Map<string, string> = new Map();
        const msg = error['message'];
        result.set('status', 'FAIL');
        result.set('message', msg);
        console.error(msg);
        return result;
      }
    );
  }

  /**
   * Getter for the mission data set names list.
   * 
   * ASYNCHRONOUS METHOD TO BE USED IN THE HTML CODE.
   * IT INTERCEPTS THE RESULT TO UPDATE THE INTERNAL MISSION SERVICE STATE
   * AND THEN FORWARDS THE RESULT TO THE CALLER.
   */
  public getMissionDataSetList() {
      return new Promise((resolve, reject) => {
          this.httpClient
          .get('http://localhost:8000/missiondata/list')
          .toPromise()
          .then((res:any) => {
                // Success
                const status = res['status'];
                const message = res['message'];
                const list = res['data'];
          
                if (status == 'SUCCESS') {
                  console.log("Mission data list has been retrieved");
                  this.setMissionDataSetList(list);
                  // A new mission data set is available, update the mission data set list.
                  this.emitMissionDataSetList();
                  return list;
                } else {
                  console.error("Mission data list has not been retrieved: " + message);
                  return [];
                }
                resolve();
            },
            err => {
              // Error
              reject(err);
            }
          );
    });
  }

  /**
   * Update the list of available mission data sets by querying the back-end.
   * @returns The list of mission data sets.
   */
  public updateMissionDataSetList() {
    this.httpClient.get('http://localhost:8000/missiondata/list').subscribe((res: any) => {
      const status = res['status'];
      const message = res['message'];
      const list = res['data'];

      if (status == 'SUCCESS') {
        console.log("Mission data list has been retrieved");
        this.setMissionDataSetList(list);
        // A new mission data set is available, update the mission data set list.
        this.emitMissionDataSetList();
      } else {
        console.error("Mission data list has not been retrieved: " + message);
      }
    });
  } 


  /****************************************************************************
   * VTS VISUALIZATION: GENERAL AND COVERAGE REQUESTS
   */

  /**
    * Build a JSatOrb REST HTTP request using all the satellites and ground stations existing in the current mission data set.
    * 
    * @param options VTS data generation options.
    */
   public buildJSatOrbRESTRequestWithOptions(options: any) {
    return this.buildJSatOrbRESTRequest(this.satelliteList, this.constellationList, this.groundStationList, options);
  }

  /**
   * Build the Satellite and constellations list according to the coverage settings selected by the user.
   * Remark: 
   * - if the user selected the satellite mode, then:
   *       . the constellations list is empty,
   *       . the satellites list only contains the satellite selected by the user.
   * - if the user selected the constellation mode:
   *       . the constellations list contains only the constellation selected by the user,
   *       . the satellites list only contains the satellites of the constellation selected by the user.
   * 
   * @returns a tuple of the (satellites list, constellations list).
   */
  public getCoverageSettingsSatellitesAndConstellations() : [SatelliteModel[], ConstellationModel[]] {     
   const cs = this.getCoverageSettings();

   // Prepare the satellites/constellations, according to the user coverage settings.
   let satList: SatelliteModel[] = [];
   let consList: ConstellationModel[] = [];
   if (cs.satConstMode == 'satellite') {
     const sat = this.getSatelliteByName(cs.satConstName);
     satList = [ sat ];
   } else {
     const cons = this.getConstellationByName(cs.satConstName);
     consList = [ cons ];
     // Build the list of satellites
     cons.satellites.forEach(sat => {
       satList.push(this.getSatelliteByName(sat));
     });
   }

   return [satList, consList];
 }

   /**
   * Build a JSatOrb REST HTTP request for the coverage functionnality using the 
   * current Coverage settings state.
   */
  public buildJSatOrbRESTRequestWithCoverageSettings() {

   // Get the coverage settings' options part.
   const csOptions = CoverageService.getCoverageSettingsAsJSONOptions(this.getCoverageSettings());

   // Get the satellites and constellations list according to the coverage settings.
   const csSatConst = this.getCoverageSettingsSatellitesAndConstellations();

   const satellites = csSatConst[0];
   const constellations = csSatConst[1];

   var ts = new TimeSettingsModel(new Date("1970-01-01T00:00:00"), new Date("1970-01-02T00:00:00"), 60);
   return this.buildJSatOrbRESTRequestWithTimeSettings(ts, satellites, constellations, this.groundStationList, csOptions);
 }

   /**
   * Build a JSatOrb REST HTTP request.
   * 
   * @param satellites Array of Satellites to process.
   * @param groundStations Array of Ground stations to process.
   * @param options VTS data generation options.
   */
 public buildJSatOrbRESTRequestWithTimeSettings(ts: TimeSettingsModel, satellites: SatelliteModel[], constellations: ConstellationModel[], groundStations: GroundStationModel[], options: any) {
     let req;
     const mission = this.getCurrentMission();
     const cb = this.getCelestialBody();

     req = {
       'header': {
         'mission': mission,
         'celestialBody': cb,
         'timeStart': ts.getStringFormattedStartDate(),
         'timeEnd': ts.getStringFormattedEndDate(),
         'step': ts.stepTime
       },
       'satellites': satellites,
       'constellations': constellations,
       'groundStations': groundStations,
       'options': options
     };
     return req;
 }
 
   /**
   * Build a JSatOrb REST HTTP request.
   * 
   * @param ts provided time settings.
   * @param satellites Array of Satellites to process.
   * @param groundStations Array of Ground stations to process.
   * @param options VTS data generation options.
   */
  public buildJSatOrbRESTRequest(satellites: SatelliteModel[], constellations: ConstellationModel[], groundStations: GroundStationModel[], options: any) {
    return this.buildJSatOrbRESTRequestWithTimeSettings(this.getTimeSettings(), satellites, constellations, groundStations, options);
  }

}


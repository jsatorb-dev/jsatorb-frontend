import { formatDate } from '@angular/common';

/**
 * This service contains all JsatOrb constants and some general purposes methods.
 */
export class ConstantsService {

  /**
   * Mission's celestial body: EARTH by default.
   */
  static DEFAULT_CELESTIAL_BODY: string = 'EARTH';

  /**
   * List of available celestial bodies based on what the Orekit library Celestial Body factory pre-defines.
   */
  static CELESTIAL_BODIES: string[] = ['EARTH', 'JUPITER', 'MARS', 'MERCURY', 'MOON', 'NEPTUNE', 'PLUTO', 'SATURN', 'SUN', 'URANUS', 'VENUS'];

  /**
   * List of available pre-defined attitude laws.
   * 
   * The list of those available values are defined in the jsatorb-rest-api git repository, in the document at: 
   *    jsatorb-rest-api.git/listAttitudes.md.
   * At the moment, the available values in the JSatOrb GUI are those defined below.
   */
  static ATTITUDE_LAWS: string[] = ['LOF_LVLH', 'LOF_TNW', 'LOF_VNC', 'LOF_VVLH', 'POINTING_CENTRAL', 'POINTING_SUN', 'NADIR'];

  /**
   * Description of attitude laws.
   */
  static ATTITUDE_LAWS_DESCRIPTIONS = {
    'LOF_LVLH': "LVLH frame: X // Position, Z // Orbital momentum (same as QSW frame)",
    'LOF_TNW': "TNW frame: X // Velocity, Z // Orbital momentum",
    'LOF_VNC': "VNC: Velocity - Normal - Co-normal frame (X // Velocity, Y // Orbital momentum)",
    'LOF_VVLH': "VVLH: Vehicle Velocity, Local Horizontal frame (Z // -Position, Y // -Orbital momentum)",
    'POINTING_CENTRAL': "BODY CENTER: Z axis pointing towards central body",
    'POINTING_SUN': "SUN: Z axis pointing towards Sun",
    'NADIR': "NADIR: Z axis pointing towards the vertical of the ground point under satellite",
  };

  // Format a date to ISO-8601 standard format.
  static formatDateISO8601(date: Date): string {
    return formatDate(date, 'yyyy-MM-ddTHH:mm:ss', 'en-GB');
  }
}

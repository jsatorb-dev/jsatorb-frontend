import { RegionModel } from "src/app/Models/Region.model";
import { CropperPositionModel } from "src/app/Models/CropperPosition.model";
import { VtsCoverageVisualizationFormComponent } from "src/app/Components/vts-coverage-visualization-form/vts-coverage-visualization-form.component";
import { CropperPosition } from "ngx-image-cropper";
import { CoverageSettingsModel } from "src/app/Models/CoverageSettings.model";

/**
 * This service contains helper constants and methods to implement
 * the Coverage functionnality in the JSatOrb GUI.
 */
export class CoverageService {

  // Coverage Region selected by default.
  static DEFAULT_REGION : RegionModel = new RegionModel(-90.0, 90.0, -180.0, 180.0);

  // Coverage Region selected by default.
  static DEFAULT_CROPPER : CropperPositionModel = CoverageService.convertRegionModelToCropperPosition(CoverageService.DEFAULT_REGION);

  /**
   * Convert a cropper position into a region.
   * @param cp The cropper position.
   */
  static convertCropperPositionToRegionModel(cp: CropperPosition): RegionModel {
    return new RegionModel(90 - cp.y2 /2, 90 - cp.y1 /2, cp.x1 /2 -180, cp.x2 /2 -180);
  }

  /**
   * Convert a region into a cropper position.
   * @param r The region.
   */
  static convertRegionModelToCropperPosition(r: RegionModel): CropperPosition {
    const x1 = Math.round((r.lonMin + 180) * 2);
    const x2 = Math.round((r.lonMax + 180) * 2);
    const y1 = Math.round((90 - r.latMax) * 2);
    const y2 = Math.round((90 - r.latMin) * 2);
    return new CropperPositionModel(x1, y1, x2, y2);
  }

   /**
    * Build the tree/JSON representation of the coverage settings 
    * conforming to the REST API requests format (in the 'options' part).
    * 
    * @param cs The coverage settings to convert.
    * 
    * @returns The Coverage settings JSON tree part of the options.
    */
   public static getCoverageSettingsAsJSONOptions(cs : CoverageSettingsModel): object {

    const DR = CoverageService.DEFAULT_REGION;

    let start: string = undefined;                  // Start date
    let end: string = undefined;                    // End date
    let step: number = undefined;                   // Time step
    let me: number = undefined;                     // minElevation
    let nstc: number = undefined;                   // Nb of sats to cover a point
    let scm: string = undefined;                    // Sat/Constellation mode
    let scn: string = undefined;                    // Sat/constellation name
    let rLa: number[] = [ DR.latMin, DR.latMax ];   // Region latitudes
    let rLo: number[] = [ DR.lonMin, DR.lonMax ];   // Region longitudes
    let pm: string = undefined;                     // Plot mode


    // Prevent undefined time settings to raise errors
    if (cs !== undefined) {
      if (cs.dateStart !== undefined && cs.dateStart.toString() !== "") {
        start = cs.getStringFormattedStartDate();
      }
      if (cs.dateEnd !== undefined && cs.dateEnd.toString() !== "") {
        end = cs.getStringFormattedEndDate();
      }
      
      step = cs.step;
      me = cs.minElevation;
      nstc = cs.nbSatsToCover;
      scm = cs.satConstMode;
      scn = cs.satConstName;
      rLa = [cs.region.latMin, cs.region.latMax];
      rLo = [cs.region.lonMin, cs.region.lonMax];
      pm = cs.plotMode;
    }


    // Build the coverage options JSON data structure.
    const csOptions = {
      'COVERAGE': {
        'timeStart': start,
        'timeEnd': end,
        'step': step,
        'elevation': me,
        'nbSatsToCover': nstc,
        'satConstMode': scm,
        'satConstName': scn,
        'regionLatitudes': rLa,
        'regionLongitudes': rLo,
        'plotType': pm
      }
    }
    return csOptions;
   }

   /**
    * Read the coverage settings from their tree/JSON representation 
    * conforming to the REST API requests format (in the 'options' part).
    * 
    * @param co The coverage options part to convert.
    * 
    * @returns The Coverage settings.
    */
   public static getCoverageSettingsFromJSONOptions(co : object): CoverageSettingsModel {

    let start: any = undefined;              // Start date
    let end: any = undefined;                // End date
    let step: number = undefined;               // Time step
    let me: number = undefined;                 // minElevation
    let nstc: number = undefined;               // Nb of sats to cover a point
    let scm: string = undefined;                // Sat/Constellation mode
    let scn: string = undefined;                // Sat/constellation name
    let latMin, latMax: number = undefined;     // Region latitudes
    let lonMin, lonMax: number = undefined;     // Region longitudes
    let pm: string = undefined;                 // Plot mode

    // Get back each sub-property from the JSON object.
    Object.keys(co).forEach( 
      (subkey) => {
        if (subkey == 'timeStart') {
            start = co[subkey];
        } else if (subkey == 'timeEnd') {
            end = co[subkey];
        } else if (subkey == 'step') {
            step = co[subkey];
        } else if (subkey == 'elevation') {
            me = co[subkey];
        } else if (subkey == 'nbSatsToCover') {
            nstc = co[subkey];
        } else if (subkey == 'satConstMode') {
            scm = co[subkey];
        } else if (subkey == 'satConstName') {
            scn = co[subkey];
        } else if (subkey == 'regionLatitudes') {
            latMin = co[subkey][0];
            latMax = co[subkey][1];
        } else if (subkey == 'regionLongitudes') {
            lonMin = co[subkey][0];
            lonMax = co[subkey][1];
        } else if (subkey == 'plotType') {
            pm = co[subkey];
        }
      }
    );

    // Create updated instance of the coverage settings.
    const cs : CoverageSettingsModel = 
    new CoverageSettingsModel(
        start, 
        end, 
        step,
        me, 
        nstc, 
        scm,
        scn, 
        new RegionModel(latMin, latMax, lonMin, lonMax), 
        pm);

    return cs;
   }
}
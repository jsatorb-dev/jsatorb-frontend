import {Directive, ElementRef, OnDestroy, OnInit} from '@angular/core';
import {MissionService} from '../Services/mission/mission.service';
import {CzmlService} from '../Services/czml/czml.service';
import {Subscription} from 'rxjs';

@Directive({
  selector: '[appCesium]'
})
export class CesiumDirective implements OnInit, OnDestroy {
  czmlDataSubscription: Subscription;
  constructor(private el: ElementRef, private missionService: MissionService, private testService: CzmlService) {
  }
  ngOnInit() {
    let czmlStream = new Cesium.CzmlDataSource();
    const viewer = new Cesium.Viewer(this.el.nativeElement, {
      skyBox: false,
      shouldAnimate: true,
      imageryProvider : Cesium.createTileMapServiceImageryProvider({
        url : Cesium.buildModuleUrl('Assets/Textures/NaturalEarthII')
      }),
      baseLayerPicker: false,
      geocoder: false,
      viewHomeButton: false
    });
    // this.renderer.setElementStyle(this.el.nativeElement, 'color', '#FFFFFF');
    viewer.dataSources.add(czmlStream.load([]
    ));
    this.czmlDataSubscription = this.testService.czmlDataSubject.subscribe((data) => {
      czmlStream.load(data);
      console.log(data);
    });
    viewer.scene.globe.enableLighting = true;
    this.testService.emitCzmlData();
  }

  ngOnDestroy() {
   this.czmlDataSubscription.unsubscribe();
  }
}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import {
  MatGridListModule,
  MatCardModule,
  MatMenuModule,
  MatIconModule,
  MatButtonModule,
  MatToolbarModule,
  MatSidenavModule,
  MatListModule,
  MatTreeModule,
  MatOptionModule,
  MatSelectModule,
  MatTableModule,
  MatDialogModule,
  MatTooltipModule,
  MatFormFieldModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatRadioModule,
  MatCheckboxModule,
  MatInputModule,
  MatPaginatorModule,
  MAT_DATE_LOCALE
} from '@angular/material';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MapComponent } from './Components/map/map.component';
import { ResponsiveNavigationComponent } from './Components/responsive-navigation/responsive-navigation.component';
import { LayoutModule } from '@angular/cdk/layout';
import { RouterModule, Routes } from '@angular/router';
import { SatelliteFormComponent } from './Components/satellite-form/satellite-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { GroundStationFormComponent } from './Components/ground-station-form/ground-station-form.component';
import { MissionService } from './Services/mission/mission.service';
import { EclipseComponent } from './Components/eclipse/eclipse.component';
import { LinkBudgetFormComponent } from './Components/link-budget-form/link-budget-form.component';
import { EclipseChartComponent } from './Components/eclipse-chart/eclipse-chart.component';
import { CzmlService} from './Services/czml/czml.service';
import { CesiumDirective } from './cesium-directive/cesium.directive';
import { CelestrakTleComponent } from './Components/celestrak-tle/celestrak-tle.component';
import { NgxNanospaceClientLibModule } from 'ngx-nanospace-client-lib';
import { NgxMatDatetimePickerModule, NgxMatTimepickerModule, NgxMatNativeDateModule, NgxMatDateFormats, NGX_MAT_DATE_FORMATS } from '@angular-material-components/datetime-picker';
import { registerLocaleData } from '@angular/common';
import localeGB from '@angular/common/locales/en-GB';
registerLocaleData(localeGB, "gb");
import { DateConversionComponent } from './Components/date-conversion/date-conversion.component';
import { MissionMgmtComponent } from './Components/mission-mgmt/mission-mgmt.component';
import { ConstellationFormComponent } from './Components/constellation-form/constellation-form.component';
import { VtsVisualizationFormComponent } from './Components/vts-visualization-form/vts-visualization-form.component';
import { BaseDisplayMessageComponent } from './Components/base-display-message/base-display-message.component';
import { ConfirmationDialogComponent } from './Components/confirmation-dialog/confirmation-dialog.component';
import { VtsCoverageVisualizationFormComponent } from './Components/vts-coverage-visualization-form/vts-coverage-visualization-form.component';
import { ImageCropperModule } from 'ngx-image-cropper';
import { ConstellationWizardFormComponent } from './Components/constellation-wizard-form/constellation-wizard-form.component';
import { NgxMatMomentModule } from '@angular-material-components/moment-adapter';

const config = {
  endpoint: 'https://dcas-nanostar.isae.fr/api/'
};

// This constant is used to declare the input nd display date/time formats
// used by the Angular Material DateTime picker widgets.
const JSO_CUSTOM_DATE_FORMATS: NgxMatDateFormats = {
  parse: {
    // Input format
    dateInput: "DD/MM/YYYY, HH:mm:ss"
  },
  display: {
    // Display format
    dateInput: "DD/MM/YYYY, HH:mm:ss",
    // Label in the upper left corner of the date/time picker popup.
    monthYearLabel: "MMM YYYY",
    // Accessibility attributes: see https://a11yproject.com/
    dateA11yLabel: "LL",
    monthYearA11yLabel: "MMMM YYYY"
  }
};

const appRoutes: Routes = [
  { path: 'satellite', component: SatelliteFormComponent },
  { path: 'satellite/:name', component: SatelliteFormComponent },
  { path: 'constellation', component: ConstellationFormComponent},
  { path: 'constellation/:name', component: ConstellationFormComponent},
  { path: 'constellationWizard', component: ConstellationWizardFormComponent},
  { path: 'groundStation', component: GroundStationFormComponent},
  { path: 'groundStation/:name', component: GroundStationFormComponent},
  { path: 'eclipse', component: EclipseComponent},
  { path: 'celestrak', component: CelestrakTleComponent},
  { path: 'dateConversion', component: DateConversionComponent},
  { path: 'missionMgmt', component: MissionMgmtComponent},
  { path: 'vtsVisualization', component: VtsVisualizationFormComponent},
  { path: 'vtsCoverageVisualization', component: VtsCoverageVisualizationFormComponent},
  { path: '', component: MissionMgmtComponent},
  { path: 'linkBudget', component: LinkBudgetFormComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    MapComponent,
    ResponsiveNavigationComponent,
    SatelliteFormComponent,
    GroundStationFormComponent,
    EclipseComponent,
    LinkBudgetFormComponent,
    EclipseChartComponent,
    CesiumDirective,
    CelestrakTleComponent,
    DateConversionComponent,
    MissionMgmtComponent,
    ConstellationFormComponent,
    VtsVisualizationFormComponent,
    BaseDisplayMessageComponent,
    ConfirmationDialogComponent,
    VtsCoverageVisualizationFormComponent,
    ConstellationWizardFormComponent
  ],
  imports: [
    BrowserModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatTreeModule,
    MatOptionModule,
    MatSelectModule,
    MatTableModule,
    MatDialogModule,
    MatTooltipModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRadioModule,
    MatCheckboxModule,
    LayoutModule,
    ReactiveFormsModule,
    MatInputModule,
    NgxMatDatetimePickerModule,
    NgxMatTimepickerModule,
    NgxMatNativeDateModule,
    NgxMatMomentModule,    
    FormsModule,
    HttpClientModule,
    MatPaginatorModule,
    RouterModule.forRoot(appRoutes),
    NgxNanospaceClientLibModule.forRoot(config),
    ImageCropperModule
  ],
  entryComponents: [
    ConfirmationDialogComponent
  ],
  providers: [
  // This provider is set to change the DateTime widget from default OS american Locale 
  // to british Locale for date presentation conerns (day/month order issue).
  { provide: NGX_MAT_DATE_FORMATS, useValue: JSO_CUSTOM_DATE_FORMATS },
    MissionService, 
    CzmlService
  ],
  bootstrap: [AppComponent]
})

export class AppModule {}


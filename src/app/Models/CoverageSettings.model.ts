import { RegionModel } from './Region.model';

/**
 * This model represents the whole Coverage functionnality settings.
 *
 */
export class CoverageSettingsModel {
    /**
     * CoverageSettingsModel constructor.
     * @param dateStart Coverage's processing Horizon: starting date.
     * @param dateEnd Coverage's processing Horizon: ending date.
     * @param step Time step for the Coverage horizon processing.
     * @param minElevation Minimum elevation.
     * @param nbSatsToCover Minimum number of visible satellites to consider an area as covered.
     * @param satConstMode Satellite or constellation input mode.
     * @param satConstName Name of unique Satellite or constellation name to process.
     * @param region The geographical area to process.
     * @param plotMode The plot mode requested by the user.
     */
  constructor(
    public dateStart: Date,
    public dateEnd: Date,
    public step: number,
    public minElevation: number,
    public nbSatsToCover: number,
    public satConstMode: string,
    public satConstName: string,
    public region: RegionModel,
    public plotMode: string) {
  }

  /**
   * Get the starting date as an ISO formatted string.
   */
  public getStringFormattedStartDate(): string {
    const temp = new Date(this.dateStart).toISOString();
    return temp.slice(0, temp.length - 5);
  }

  /**
   * Get the ending date as an ISO formatted string.
   */
  public getStringFormattedEndDate(): string {
    const temp = new Date(this.dateEnd).toISOString();
    return temp.slice(0, temp.length - 5);
  }
}
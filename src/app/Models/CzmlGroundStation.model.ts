export class CzmlGroundStationModel {
  // "id":"Facility/AGI",
  // "name":"AGI",
  // "availability":"2012-03-15T10:00:00Z/2012-03-16T10:00:00Z",
  // "description":"<!--HTML-->\r\n<p>\r\nAnalytical Graphics, Inc. (AGI) develops commercial modeling and analysis software used by more than 40,000 global space, defense and intelligence professionals. AGI founded Cesium to meet the need for a cross-platform virtual globe with dynamic-data visualization. AGI continues to invest heavily in Cesium's development and facilitate the growth of the user and contributor community.\r\n</p>",
  // "billboard":{
  //   "image":"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAACvSURBVDhPrZDRDcMgDAU9GqN0lIzijw6SUbJJygUeNQgSqepJTyHG91LVVpwDdfxM3T9TSl1EXZvDwii471fivK73cBFFQNTT/d2KoGpfGOpSIkhUpgUMxq9DFEsWv4IXhlyCnhBFnZcFEEuYqbiUlNwWgMTdrZ3JbQFoEVG53rd8ztG9aPJMnBUQf/VFraBJeWnLS0RfjbKyLJA8FkT5seDYS1Qwyv8t0B/5C2ZmH2/eTGNNBgMmAAAAAElFTkSuQmCC",
  //   "scale":1.5,
  //   "show":true,
  // },
  // "position":{
  //   "cartographicDegrees":[
  //     1.4436700, 43.6042600,2
  //     ]
  // }
  id: string;
  name: string;
  availability: string;
  description: string;
  billboard: any;
  position: any;
  constructor(name: string, longitude: Number, latitude: Number, altitude: Number, timeInterval?: string) {
    this.id = 'Station/' + name;
    this.name = name;
    this.availability = timeInterval;
    this.billboard = {
      /**
       * Image Correspond à l'encodage en base64 de l'icone affiché
       */
      'image': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEh' +
        'ZcwAADsMAAA7DAcdvqGQAAACvSURBVDhPrZDRDcMgDAU9GqN0lIzijw6SUbJJygUeNQgSqepJTyHG91LVVpwDdfxM3T9TSl1EXZvDwii471fivK73cBFFQNTT/' +
        'd2KoGpfGOpSIkhUpgUMxq9DFEsWv4IXhlyCnhBFnZcFEEuYqbiUlNwWgMTdrZ3JbQFoEVG53rd8ztG9aPJMnBUQf/VFraBJeWnLS0RfjbKyLJA8FkT5seDYS1' +
        'Qwyv8t0B/5C2ZmH2/eTGNNBgMmAAAAAElFTkSuQmCC',
      'scale': 1.5,
      'show': true,
    };
    this.position =  {
      'cartographicDegrees': [Number(longitude), Number(latitude), Number(altitude)]
    };

  }

  getDescription(): string {
    return this.description;
  }

  setDescription(descr: string) {
    this.description = descr;
  }
}

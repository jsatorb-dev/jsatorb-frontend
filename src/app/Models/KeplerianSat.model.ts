import {SatelliteModel} from './Satellite.model';

export class KeplerianSatModel extends SatelliteModel {
  constructor(
    public name: string,        // Satellite name
    public type: string,        // Satellite type: keplerian
    public sma: number,         // Semi-major axis
    public ecc: number,         // Excentricity
    public inc: number,         // Inclination
    public pa: number,          // Argument of perigee
    public raan: number,        // Right ascension of ascending node
    public meanAnomaly: number, // Mean anomaly
    public color?: string       // Satellite optional color
    ) {  
    super(name, type, color);
  }
}

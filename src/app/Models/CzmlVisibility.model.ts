import {timeInterval} from 'rxjs/operators';

export class CzmlVisibilityModel {
  id;
  name;
  availability = [];
  polyline;

  constructor(groundStationName: string, satName: string, timeIntervalArray: any[]) {
    this.id = 'Station/' + groundStationName + '-to-Satellite/' + satName;
    this.name = groundStationName + ' to ' + satName;
    const availibilityArray: string[] = [];
    const showAvaibilityArray = [{
      interval: '0000-01-01T00:00:00Z/' + this.formatStringTime(timeIntervalArray[0].startDate),
      boolean: false
    }];

    timeIntervalArray.forEach((el, index) => {
      // On parcours les éléments du tableau pour générer une liste des periodes visibilités,
      // Ainsi qu'une liste d'affichage de la ligne qui va lier le satellite et la station sols
      // Cette liste contient des objets du type { interval: 'timeIsoFormat', boolean: false
      const intervalVisible = this.getStringInterval(this.formatStringTime(el.startDate), this.formatStringTime(el.endDate));

      this.availability.push(intervalVisible); // fenetre des visibilités

      // Affichage de la ligne entre satellite et stations sols
      showAvaibilityArray.push({
        interval: intervalVisible,
        boolean: true
      });
      if (timeIntervalArray[index + 1] !== undefined) { // On verifie que la peride regardé ne soit pas la derniére periode de visibilité
        // Si ce n'est pas le cas alors on ajoute la prochaine periode de non visibilité
        const intervalNotVisible = this.getStringInterval(this.formatStringTime(el.endDate),
          this.formatStringTime(timeIntervalArray[index + 1].startDate));
        showAvaibilityArray.push({
          interval: intervalNotVisible,
          boolean: false
        });
      }
    });
    this.polyline = {
      show: showAvaibilityArray,
      'width': 1,
      'material': {
        'solidColor': {
          'color': {
            'rgba': [
              0, 255, 255, 255
            ]
          }
        }
      },
      positions: {
        references: [
          'Station/' + groundStationName + '#position', 'Satellite/' + satName + '#position'
        ]
      }
    };
  }

  // private formatStringTime(date: Date): string {
  //   const dateToString: string = date.toISOString();
  //   return dateToString.substr(0, dateToString.length - 5) + 'Z';
  // }
  //
  // private getStringInterval(date1: Date, date2: Date): string {
  //   return this.formatStringTime(date1) + '/' + this.formatStringTime(
  //     new Date(date2));
  // }

  private getStringInterval(date1: string, date2: string): string {
    return date1 + '/' + date2;
  }

  private formatStringTime(date: string): string {
    const splitedDate = date.split(' ');
    return splitedDate[0] + 'T' + splitedDate[1] + 'Z';
  }

}

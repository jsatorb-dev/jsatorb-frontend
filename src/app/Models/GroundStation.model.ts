export class GroundStationModel {
  constructor(
    public name: string,      // Ground station name
    public latitude: number,  // Ground station latitude (degrees)
    public longitude: number, // Ground station longitude (degrees)
    public altitude: number,  // Ground station altitude (meters)
    public elevation: number  // Ground station elevation (degrees)
    ) {
  }
}

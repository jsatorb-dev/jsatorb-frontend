export class CzmlDocumentModel {
  // {
  // "id":"document",
  // "name":"simple",
  // "version":"1.0",
  // "clock":{
  //   "interval":"2012-03-15T10:00:00Z/2012-03-16T10:00:00Z",
  //   "currentTime":"2012-03-15T10:00:00Z",
  //   "multiplier":60,
  //   "range":"LOOP_STOP",
  //   "step":"SYSTEM_CLOCK_MULTIPLIER"
  // }
  id: string;
  name: string;
  version: string;
  clock: any;
  constructor(timeStart: Date, timeStop: Date, interval: Number) {
    this.id = 'document';
    this.name = 'simple';
    this.version = '1.0';
    this.clock = {
      'interval': this.formatStringTimeInterval(timeStart, timeStop), // Le format de la date est peut être mauvais
      'currentTime': this.formatStringTime(timeStart),
      'multiplier': 1,
      'range': 'LOOP_STOP',
      'step': 'SYSTEM_CLOCK_MULTIPLIER'
    };
  }

  private formatStringTime(date: Date): string {
    const dateToString: string = date.toISOString();
    return dateToString.substr(0, dateToString.length - 5) + 'Z';
  }

  private formatStringTimeInterval(startDate: Date, stopDate: Date): string {
    return this.formatStringTime(startDate) + '/' + this.formatStringTime(stopDate);
  }

}

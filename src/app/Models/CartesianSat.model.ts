import {SatelliteModel} from './Satellite.model';

export class CartesianSatModel extends SatelliteModel {
  constructor(
    public name: string,  // Satellite name
    public type: string,  // Satellite type: cartesian
    public x: number,     // Satellite position: X component
    public y: number,     // Satellite position: Y component
    public z: number,     // Satellite position: Z component
    public vx: number,    // Satellite velocity: X component
    public vy: number,    // Satellite velocity: y component
    public vz: number,    // Satellite velocity: z component
    color?: string        // Satellite optional color
    ) {
    super(name, type, color);
  }
}

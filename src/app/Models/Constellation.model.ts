export class ConstellationModel {
    constructor(
      public name: string,          // Constellation name
      public satellites: string[]   // List of satellite names
      ) {
    }
  }
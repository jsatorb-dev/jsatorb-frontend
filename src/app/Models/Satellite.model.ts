export class SatelliteModel {
  constructor(
    public name: string,    // Satellite name
    public type: string,    // Satellite type: TLE, keplerian, cartesian
    public color?: string   // Satellite optional color
    ) {
  }
}

export class CzmlSatelliteModel {
  id;
  name;
  availability;
  description = '';
  billboard;
  label;
  path;
  position;

  constructor(name: string, cartesianPos: any[], startDate: string, availability: string, color?: string, description?: string) {
    let pathColor;
    this.id = 'Satellite/' + name;
    this.name = name;
    this.availability = availability;
    if (description) {
      this.description = description;
    } else {
      this.description = '';
    }
    if (color) {
      pathColor = this.hexToRgbA(color);
    } else {
      pathColor = [
        255, 0, 255, 255
      ];
    }
    this.billboard = {
      'image': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAD' +
        'sMAAA7DAcdvqGQAAADJSURBVDhPnZHRDcMgEEMZjVEYpaNklIzSEfLfD4qNnXAJSFWfhO7w2Zc0Tf9QG2rXrEzSUeZLOGm47WoH95x3Hl3jEgilvDgsOQUTqsNl68' +
        'ezEwn1vae6lceSEEYvvWNT/Rxc4CXQNGadho1NXoJ+9iaqc2xi2xbt23PJCDIB6TQjOC6Bho/sDy3fBQT8PrVhibU7yBFcEPaRxOoeTwbwByCOYf9VGp1BYI1BA+' +
        'EeHhmfzKbBoJEQwn1yzUZtyspIQUha85MpkNIXB7GizqDEECsAAAAASUVORK5CYII=',
      'scale': 1.5,
      'show': true,
      'verticalOrigin': 'CENTER'
    };

    this.label = {
      'fillColor': {
        'rgba': [
          255, 0, 255, 255
        ]
      },
      'font': '11pt Lucida Console',
      'horizontalOrigin': 'LEFT',
      'pixelOffset': {
        'cartesian2': [
          12, 0
        ]
      },
      'show': true,
      'text': this.name,
    };
    this.path = {
      'width': 1
      ,
      'material': {
        'solidColor': {
          'color': {
            'rgba': pathColor
          }
        }
      }
    };

    this.position = {
      'interpolationAlgorithm': 'LAGRANGE',
      'interpolationDegree': 5,
      'referenceFrame': 'INERTIAL',
      'epoch': startDate + 'Z',
      'cartesian': cartesianPos,
    };
  }

  hexToRgbA(hex) {
    let c;
    if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
      c = hex.substring(1).split('');
      if (c.length === 3) {
        c = [c[0], c[0], c[1], c[1], c[2], c[2]];
      }
      c = '0x' + c.join('');
      return [(c >> 16) & 255, (c >> 8) & 255, c & 255, 255];
    }
    throw new Error('Bad Hex');
  }
}

import {SatelliteModel} from './Satellite.model';

export class TleSatModel extends SatelliteModel {
  constructor(
    public name: string,  // Satellite name
    public type: string,  // Satellite type: TLE
    public line1: string, // First line
    public line2: string, // Second line
    color?: string        // Optional satellite color
    ) {
    super(name, type, color);
  }
}

/**
 * This model represents a geographical "spherical rectangle" region defined by a 
 * range of latitudes and longitudes.
 */
export class RegionModel {
    /**
     * RegionModel constructor.
     * @param latMin The "spherical rectangle's" top limit.
     * @param latMax The "spherical rectangle's" bottom limit.
     * @param lonMin The "spherical rectangle's" left limit.
     * @param lonMax The "spherical rectangle's" right limit.
     */
    constructor(
      public latMin: number,
      public latMax: number,
      public lonMin: number,
      public lonMax: number
      ) {
    }
  }
import {Time} from '@angular/common';

export class TimeSettingsModel {
  duration: number;

  constructor(
    public dateStart: Date, // Starting date
    public dateEnd: Date,   // Ending date
    public stepTime: number // Time step (seconds)
    ) {

    // Get the duration between starting and ending dates in seconds.
    if (dateStart !== null && dateStart !== undefined && dateEnd !== null && dateEnd !== undefined) {
      this.duration = Math.round((new Date(dateEnd).getTime() - new Date(dateStart).getTime()) / 1000);
    } else {
      this.duration = 0;
    }

  }

  public setStartingDate(startingDate: Date):void {
    this.dateStart = startingDate;
  }

  public getStartingDate(): Date {
    return this.dateStart;
  }

  public getStringFormattedStartDate(): string {
    const temp = new Date(this.dateStart).toISOString();
    return temp.slice(0, temp.length - 5);
  }

  public setEndingDate(endingDate: Date):void {
    this.dateEnd = endingDate;
  }
  
  public getEndingDate(): Date {
    return this.dateEnd;
  }

  public getStringFormattedEndDate(): string {
    const temp = new Date(this.dateEnd).toISOString();
    return temp.slice(0, temp.length - 5);
  }

  private formatStringTime(date: Date): string {
    const dateToString: string = date.toISOString();
    return dateToString.substr(0, dateToString.length - 5) + 'Z';
  }
}

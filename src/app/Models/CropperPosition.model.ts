import { CropperPosition } from 'ngx-image-cropper';

/**
 * This model represents a Cropper Position implementation of the 'ngx-image-cropper' module's
 * CropperPosition interface.
 *
 */
export class CropperPositionModel implements CropperPosition {
    /**
     * CropperPositionModel constructor.
     * @param x1 The rectangle's left limit.
     * @param y1 The rectangle's top limit.
     * @param x2 The rectangle's right limit.
     * @param y2 The rectangle's bottom limit.
     */
  constructor(
    public x1: number,
    public y1: number,
    public x2: number,
    public y2: number) {
  }
}
